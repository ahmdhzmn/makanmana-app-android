package com.makanmanaapp;

public class Restaurant {

    private String ImageUrl, RestaurantName, RestaurantCategory;

    public Restaurant() {

    }

    public Restaurant(String imageUrl, String restaurantName, String restaurantCategory) {
        ImageUrl = imageUrl;
        RestaurantName = restaurantName;
        RestaurantCategory = restaurantCategory;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getRestaurantName() {
        return RestaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        RestaurantName = restaurantName;
    }

    public String getRestaurantCategory() {
        return RestaurantCategory;
    }

    public void setRestaurantCategory(String restaurantCategory) {
        RestaurantCategory = restaurantCategory;
    }
}
