package com.makanmanaapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class NewFacebookUserActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    DatabaseReference RootRef = FirebaseDatabase.getInstance().getReference();

    private Button reloginButton;
    private ImageButton submitUsername;
    private EditText usernameInput;

    private LinearLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_facebook_user);

        mainLayout = findViewById(R.id.new_fb_user_main_layout);

        usernameInput = findViewById(R.id.create_username_input);
        submitUsername = findViewById(R.id.submit_username_button);

        RoundCornerDrawable round = new RoundCornerDrawable(BitmapFactory.decodeResource(getResources(),R.drawable.background_card),
                getResources().getDimension(R.dimen.cardview_radius), 0);
        ImageView imageView = findViewById(R.id.backgroundimage);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            imageView.setBackground(round);
        else
            imageView.setBackgroundDrawable(round);

        reloginButton = findViewById(R.id.relogin_button);
        reloginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();

                Intent loginScreen = new Intent(NewFacebookUserActivity.this, LoginActivity.class);
                startActivity(loginScreen);
            }
        });

        submitUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);

                final String username = usernameInput.getText().toString().trim();

                if (TextUtils.isEmpty(username)) {
                    Toast.makeText(NewFacebookUserActivity.this,"Please enter a username!", Toast.LENGTH_LONG).show();
                } else {
                    RootRef.child("Users").orderByChild("username").equalTo(username)
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getChildrenCount() > 0) {
                                Toast.makeText(getApplicationContext(), "Username already exists. Please try other username.", Toast.LENGTH_LONG).show();
                            }
                            else {

                                sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("userName", username);
                                editor.apply();

                                String email = sharedpreferences.getString("userEmail", "");
                                String profileName = sharedpreferences.getString("profileName", "");
                                String username = sharedpreferences.getString("userName", "");
                                int userType = Integer.parseInt(sharedpreferences.getString("userType", ""));
                                String userfbid = sharedpreferences.getString("userId", "");
                                int requestedrecommendation = 0;

                                UserFB userFB = new UserFB(
                                        userType,
                                        username,
                                        profileName,
                                        email,
                                        userfbid,
                                        requestedrecommendation
                                );

                                RootRef.child("Users").child(currentUser.getUid()).setValue(userFB)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()) {

                                                    sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);

                                                    Toast.makeText(NewFacebookUserActivity.this,"Welcome " + sharedpreferences.getString("profileName", ""), Toast.LENGTH_SHORT).show();
                                                    Intent mainScreen = new Intent(NewFacebookUserActivity.this, MainActivity.class);
                                                    mainScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(mainScreen);
                                                    finish();

                                                } else {
                                                    System.out.println(task.getException());
                                                    Toast.makeText(NewFacebookUserActivity.this, "Registration Error! Please try again later.", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
            }
        });
    }
}
