package com.makanmanaapp;

public class UserFB {
    public int usertype, requestedrecommendation; //     1 - normal user     2 - fb user
    public String username, profilename, email, userfbid;

    public UserFB() {

    }

    public UserFB(int usertype, String username, String profilename, String email, String userfbid, int requestedrecommendation) {
        this.usertype = usertype;
        this.username = username;
        this.profilename = profilename;
        this.email = email;
        this.userfbid = userfbid;
        this.requestedrecommendation = requestedrecommendation;
    }


}
