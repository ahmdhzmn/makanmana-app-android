package com.makanmanaapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.RestaurantViewHolder> {

    private Context Context;
    private ArrayList<Restaurant> RestaurantArrayList;

    public RestaurantAdapter(Context context, ArrayList<Restaurant> restaurantArrayList) {
        Context = context;
        RestaurantArrayList = restaurantArrayList;
    }

    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(Context).inflate(R.layout.recommendation_display_layout, parent, false);
        return new RestaurantViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantViewHolder holder, int position) {
        Restaurant currentItem = RestaurantArrayList.get(position);

        String imageUrl = currentItem.getImageUrl();
        String restaurantName = currentItem.getRestaurantName();
        String restaurantCategory = currentItem.getRestaurantCategory();

        holder.restaurantNameView.setText(restaurantName);
        holder.restaurantCategoryView.setText(restaurantCategory);
        Picasso.get().load(imageUrl).fit().centerInside().into(holder.restaurantImageView);

    }

    @Override
    public int getItemCount() {
        return RestaurantArrayList.size();
    }

    public class RestaurantViewHolder extends RecyclerView.ViewHolder {
        public ImageView restaurantImageView;
        public TextView restaurantNameView;
        public TextView restaurantCategoryView;

        public RestaurantViewHolder(@NonNull View itemView) {
            super(itemView);
            restaurantImageView = itemView.findViewById(R.id.restaurant_image_view);
            restaurantNameView = itemView.findViewById(R.id.restaurant_name_view);
            restaurantCategoryView = itemView.findViewById(R.id.restaurant_category_view);
        }
    }
}
