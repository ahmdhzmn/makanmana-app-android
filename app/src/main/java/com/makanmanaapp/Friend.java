package com.makanmanaapp;

public class Friend {
    public String username, profilename, image;

    public Friend(){

    }

    public Friend(String username, String profilename, String image) {
        this.username = username;
        this.profilename = profilename;
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfilename() {
        return profilename;
    }

    public void setProfilename(String profilename) {
        this.profilename = profilename;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
