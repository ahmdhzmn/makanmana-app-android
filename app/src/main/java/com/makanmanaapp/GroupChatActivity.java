package com.makanmanaapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class GroupChatActivity extends AppCompatActivity {

    // Foursquare client_id and the client_secret
    final String CLIENT_ID = "N5ZKCYCWUKNJUWQ04D5Z3QTTSRFKC1BGA2EMU4CK1GTRW1QE";
    final String CLIENT_SECRET = "41WSZPKHNAOK3YJEDYVCVB4ETQTE1MPXHVGOAIVJUWNK0S4G";

    private Toolbar mToolbar;
    private ImageButton SendMessageButton;
    private EditText userMessageInput;
    private ScrollView mScrollView;
    private TextView displayTextMessage;

    private String receiveSelectedGroup;
    private int NumberOfGroupMembers;

    private String username;
    String message;

    String[][] PreferenceArray;
    String[] MainPreference, SecondPreference, ThirdPreference, FourthPreference;

    int counter = 0;


    private FirebaseAuth mAuth;
    private DatabaseReference UserRef, GroupRef, GroupMessageRef, GroupMessageKeyRef;

    private String currentUserId, currentUserName, currentDate, currentTime;

    DatabaseReference RootRef = FirebaseDatabase.getInstance().getReference();

    View splash_screen;
    RelativeLayout bottomLayout;

    UserProfilesDisplayDialog userProfilesDisplayDialog = new UserProfilesDisplayDialog();
    GenerateRecommendationDialog generateRecommendationDialog = new GenerateRecommendationDialog();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);

        receiveSelectedGroup = getIntent().getExtras().get("selectedGroupChat").toString();

        mAuth = FirebaseAuth.getInstance();
        currentUserId = mAuth.getCurrentUser().getUid();
        UserRef = FirebaseDatabase.getInstance().getReference().child("Users");
        GroupRef = FirebaseDatabase.getInstance().getReference().child("Groups").child(receiveSelectedGroup);
        GroupMessageRef = GroupRef.child("Message");

        //  Toast.makeText(GroupChatActivity.this, "Selected group: " + receiverSelectedGroup, Toast.LENGTH_LONG).show();

        //Initialize field
        mToolbar = findViewById(R.id.toolbar_group_chat);
        SendMessageButton = findViewById(R.id.send_message_button);
        userMessageInput = findViewById(R.id.input_chat_message);
        mScrollView = findViewById(R.id.my_scroll_view);
        displayTextMessage = findViewById(R.id.group_chat_text_display);
        splash_screen = findViewById(R.id.generate_recommendation_splash_screen);
        bottomLayout = findViewById(R.id.myLinearLayout);

        splash_screen.setVisibility(View.GONE);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(receiveSelectedGroup);

        GetUserInfo();

        SendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SaveMessageInfoToDatabase();

                userMessageInput.setText("");

                mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        GroupMessageRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if (dataSnapshot.exists()) {
                    DisplayChatMessages(dataSnapshot);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if (dataSnapshot.exists()) {
                    DisplayChatMessages(dataSnapshot);
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    private void GetUserInfo() {

        UserRef.child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    currentUserName = dataSnapshot.child("profilename").getValue().toString();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void SaveMessageInfoToDatabase() {
        String message = userMessageInput.getText().toString();
        String messageKey = GroupMessageRef.push().getKey();

        if (TextUtils.isEmpty(message)) {
            return;
        }
        else {
            Calendar calForDate = Calendar.getInstance();
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("dd MMM yyyy");
            currentDate = currentDateFormat.format(calForDate.getTime());

            Calendar calForTime = Calendar.getInstance();
            SimpleDateFormat currentTimeFormat = new SimpleDateFormat("hh:mm a");
            currentTime = currentTimeFormat.format(calForTime.getTime());

            HashMap<String, Object> groupMessageKey = new HashMap<>();
            GroupRef.updateChildren(groupMessageKey);

            GroupMessageKeyRef = GroupMessageRef.child(messageKey);

            HashMap<String, Object> messageInfoMap = new HashMap<>();
            messageInfoMap.put("name", currentUserName);
            messageInfoMap.put("message", message);
            messageInfoMap.put("date", currentDate);
            messageInfoMap.put("time", currentTime);

            GroupMessageKeyRef.updateChildren(messageInfoMap);
        }
    }


    private void DisplayChatMessages(DataSnapshot dataSnapshot) {
        Iterator iterator = dataSnapshot.getChildren().iterator();

        while (iterator.hasNext()) {
            final String chatDate = (String) ((DataSnapshot) iterator.next()).getValue();
            final String chatMessage = (String) ((DataSnapshot) iterator.next()).getValue();
            final String chatName = (String) ((DataSnapshot) iterator.next()).getValue();
            final String chatTime = (String) ((DataSnapshot) iterator.next()).getValue();

            RootRef.child("Users").orderByChild("profilename").equalTo(chatName).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (dataSnapshot.exists()) {
                        for(DataSnapshot item_snapshot:dataSnapshot.getChildren()) {

                            //  Log.d("Found name: ",item_snapshot.child("username").getValue().toString());
                            username = item_snapshot.child("username").getValue().toString();
                        }

                        displayTextMessage.append(Html.fromHtml("<b>"+chatName+"</b>" +
                                " <small><i>(" + username + ")</i></small>" +"<br/>" +
                                chatMessage + "<br/>" +
                                "<small><i>" + chatTime + " &nbsp; " + chatDate + "</i></small><br/><br/><br/>"));
                    } else {
                        displayTextMessage.append(Html.fromHtml("<b>"+chatName+"</b>" +
                                " <small><i>(Account no longer exist)</i></small>" +"<br/>" +
                                chatMessage + "<br/>" +
                                "<small><i>" + chatTime + " &nbsp; " + chatDate + "</i></small><br/><br/><br/>"));
                    }



                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            mScrollView.fullScroll(ScrollView.FOCUS_DOWN);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_toolbar_generate_recommendation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.generate_recommendation_option:

                mToolbar.setVisibility(View.INVISIBLE);
                bottomLayout.setVisibility(View.INVISIBLE);
                mScrollView.setVisibility(View.INVISIBLE);
                splash_screen.setVisibility(View.VISIBLE);
                //  CallFoursquareAPI();
                //  scrollView.setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        //  Call method to generate recommendation process
                        GenerateRecommendation();

                        //  splash_screen.setVisibility(View.GONE);
                        //  mToolbar.setVisibility(View.VISIBLE);
                        //  bottomLayout.setVisibility(View.VISIBLE);
                        //  mScrollView.setVisibility(View.VISIBLE);
                        //  userProfilesDisplayDialog.show(getSupportFragmentManager(), "user profiles dialog");
                    }
                }, 1500);

                //  scrollView.setVisibility(View.VISIBLE);
                return true;
                default:
                    return super.onOptionsItemSelected(item);
        }
    }

    private void GenerateRecommendation() {

        //  Count group members
        RootRef.child("Groups").child(receiveSelectedGroup).child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                NumberOfGroupMembers = Math.toIntExact(dataSnapshot.getChildrenCount());
                Log.d("Count group:", " " + NumberOfGroupMembers);

                if (NumberOfGroupMembers == 1) {
                    generateRecommendationDialog.show(getSupportFragmentManager(), "Problem generating recommendation. Group must have at least 2 members.");
                    splash_screen.setVisibility(View.GONE);
                    mToolbar.setVisibility(View.VISIBLE);
                    bottomLayout.setVisibility(View.VISIBLE);
                    mScrollView.setVisibility(View.VISIBLE);
                }
                else {
                    message = "\r\nNumber of group members: " + NumberOfGroupMembers + "\r\n\r\n";

                    PreferenceArray = new String[4][NumberOfGroupMembers];
                    MainPreference = new String[NumberOfGroupMembers];
                    SecondPreference = new String[NumberOfGroupMembers];
                    ThirdPreference = new String[NumberOfGroupMembers];
                    FourthPreference = new String[NumberOfGroupMembers];

                    //  Loop through each user in the group
                    for (DataSnapshot groupDetailSnapshot : dataSnapshot.getChildren()) {

                        final String userId = groupDetailSnapshot.getKey().toString();

                        // Get the user profiling data (main preference, second preference, third preference, fourth preference)
                        RootRef.child("Users").child(userId)
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                        message = message + "Profile Name: " + dataSnapshot.child("profilename").getValue().toString() + "\r\n";
                                        message = message + "\t\tMain Preference: " + dataSnapshot.child("Preferences").child("mainpreference").getValue().toString() + "\r\n";
                                        message = message + "\t\t2nd Preference: " + dataSnapshot.child("Preferences").child("secondpreference").getValue().toString() + "\r\n";
                                        message = message + "\t\t3rd Preference: " + dataSnapshot.child("Preferences").child("thirdpreference").getValue().toString() + "\r\n";
                                        message = message + "\t\t4th Preference: " + dataSnapshot.child("Preferences").child("fourthpreference").getValue().toString() + "\r\n\r\n";

                                        PreferenceArray[0][counter] = MainPreference[counter] = dataSnapshot.child("Preferences").child("mainpreference").getValue().toString();
                                        PreferenceArray[1][counter] = SecondPreference[counter] = dataSnapshot.child("Preferences").child("secondpreference").getValue().toString();
                                        PreferenceArray[2][counter] = ThirdPreference[counter] = dataSnapshot.child("Preferences").child("thirdpreference").getValue().toString();
                                        PreferenceArray[3][counter] = FourthPreference[counter] = dataSnapshot.child("Preferences").child("fourthpreference").getValue().toString();

                                        userProfilesDisplayDialog.receivedString(message);

                                        counter++;

                                        if (counter==NumberOfGroupMembers) {

                                            /*
                                             *
                                             * ALGORITHM PROCESS STARTS HERE
                                             *
                                             */

                                            //  Declaration
                                            counter = 0;
                                            int PreferenceScore, OccurencesInMain, OccurencesInSecond, OccurencesInThird, OccurencesInFourth, HighestScore = 0;
                                            int TopScore = 0, SecondScore = 0, ThirdScore = 0, FourthScore = 0;
                                            String PreferenceName;
                                            int[] ScoreArray;
                                            List<String> PreferenceList = new ArrayList<String>();
                                            JSONObject EachPreferenceScore = new JSONObject();
                                            JSONObject FinalPreferenceScore = new JSONObject();
                                            //  JSONObject FinalHighestScoreTest = new JSONObject();
                                            JSONArray FinalHighestScores = new JSONArray();
                                            JSONObject TopScoreJSON = new JSONObject();
                                            JSONObject SecondScoreJSON = new JSONObject();
                                            JSONObject ThirdScoreJSON = new JSONObject();
                                            JSONObject FourthScoreJSON = new JSONObject();

                                            //  Convert array into list
                                            List<String> MainPreferenceList = Arrays.asList(MainPreference);
                                            List<String> SecondPreferenceList = Arrays.asList(SecondPreference);
                                            List<String> ThirdPreferenceList = Arrays.asList(ThirdPreference);
                                            List<String> FourthPreferenceList = Arrays.asList(FourthPreference);

                                            //  Convert 2D array of PreferenceArray to List
                                            List<String> PreferenceCollections = twoDArrayToList(PreferenceArray);

                                            //  Display the users' main preference data
                                            Log.d("Users Main Preference", ": " + Arrays.toString(MainPreference));
                                            //  Display the users' second preference data
                                            Log.d("Users Second Preference", ": " + Arrays.toString(SecondPreference));
                                            //  Display the users' third preference data
                                            Log.d("Users Third Preference", ": " + Arrays.toString(ThirdPreference));
                                            //  Display the users' fourth preference data
                                            Log.d("Users Fourth Preference", ": " + Arrays.toString(FourthPreference));
                                            //  Display the users' preferences
                                            //  Log.d("Users Preferences Data", ": " + Arrays.deepToString(PreferenceArray));
                                            //  Display the users' preferences
                                            Log.d("Preference Collections", " " + PreferenceCollections);

                                            //  MANIPULATE COLLECTION OF PREFERENCES
                                            for (int i=0; i<PreferenceCollections.size(); i++) {

                                                //  Declaration score variables
                                                int mainScore = 1, secondScore = 1, thirdScore = 1, fourthScore = 1;

                                                PreferenceName = PreferenceCollections.get(i);

                                                if (i==0) {

                                                    PreferenceList.add(PreferenceName);

                                                    //  Calculate Score
                                                    if(MainPreferenceList.contains(PreferenceName)) {
                                                        OccurencesInMain = Collections.frequency(MainPreferenceList, PreferenceName);
                                                        mainScore = mainScore * (int) Math.pow(5,OccurencesInMain);
                                                    }

                                                    if(SecondPreferenceList.contains(PreferenceName)){
                                                        OccurencesInSecond = Collections.frequency(SecondPreferenceList, PreferenceName);
                                                        secondScore = (int) Math.pow(4,OccurencesInSecond);
                                                    }

                                                    if(ThirdPreferenceList.contains(PreferenceName)){
                                                        OccurencesInThird = Collections.frequency(ThirdPreferenceList, PreferenceName);
                                                        thirdScore = (int) Math.pow(3,OccurencesInThird);
                                                    }

                                                    if(FourthPreferenceList.contains(PreferenceName)){
                                                        OccurencesInFourth = Collections.frequency(FourthPreferenceList, PreferenceName);
                                                        fourthScore = (int) Math.pow(2,OccurencesInFourth);
                                                    }

                                                    PreferenceScore =  mainScore * secondScore * thirdScore * fourthScore;


                                                    //  Set top score with the first score
                                                    TopScore = PreferenceScore;

                                                    try {
                                                        TopScoreJSON.put(PreferenceName, PreferenceScore);

                                                        EachPreferenceScore.put(PreferenceName, PreferenceScore);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }

                                                } else {
                                                    //  Check if current preference already has been processed
                                                    if (!PreferenceList.contains(PreferenceName)) {

                                                        PreferenceList.add(PreferenceName);

                                                        //  Calculate Score
                                                        if(MainPreferenceList.contains(PreferenceName)) {
                                                            OccurencesInMain = Collections.frequency(MainPreferenceList, PreferenceName);
                                                            mainScore = mainScore * (int) Math.pow(5,OccurencesInMain);
                                                        }

                                                        if(SecondPreferenceList.contains(PreferenceName)){
                                                            OccurencesInSecond = Collections.frequency(SecondPreferenceList, PreferenceName);
                                                            secondScore = (int) Math.pow(4,OccurencesInSecond);
                                                        }

                                                        if(ThirdPreferenceList.contains(PreferenceName)){
                                                            OccurencesInThird = Collections.frequency(ThirdPreferenceList, PreferenceName);
                                                            thirdScore = (int) Math.pow(3,OccurencesInThird);
                                                        }

                                                        if(FourthPreferenceList.contains(PreferenceName)){
                                                            OccurencesInFourth = Collections.frequency(FourthPreferenceList, PreferenceName);
                                                            fourthScore = (int) Math.pow(2,OccurencesInFourth);
                                                        }

                                                        PreferenceScore =  mainScore * secondScore * thirdScore * fourthScore;

                                                        try {
                                                            EachPreferenceScore.put(PreferenceName, PreferenceScore);
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                        //  For first four item
                                                        //  Insert score into TopScoreJSON/SecondScoreJSON/ThirdScoreJSON/FourthScoreJSON
                                                        if (i == 1 || i == 2 || i == 3) {

                                                            //  Check if SecondScore does not have score
                                                            if (SecondScore == 0) {

                                                                //  Compare with TopScore
                                                                if (PreferenceScore > TopScore) {
                                                                    SecondScore = TopScore;
                                                                    TopScore = PreferenceScore;
                                                                    try {
                                                                        SecondScoreJSON = new JSONObject(TopScoreJSON.toString());
                                                                        TopScoreJSON = new JSONObject();
                                                                        TopScoreJSON.put(PreferenceName, PreferenceScore);
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                                else {
                                                                    SecondScore = PreferenceScore;
                                                                    try {
                                                                        SecondScoreJSON = new JSONObject();
                                                                        SecondScoreJSON.put(PreferenceName, PreferenceScore);
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }
                                                            //  Check if ThirdScore does not have score
                                                            else if (ThirdScore == 0) {

                                                                //  Compare with TopScore, SecondScore
                                                                if (PreferenceScore > TopScore) {
                                                                    ThirdScore = SecondScore;
                                                                    SecondScore = TopScore;
                                                                    TopScore = PreferenceScore;
                                                                    try {
                                                                        ThirdScoreJSON = new JSONObject(SecondScoreJSON.toString());
                                                                        SecondScoreJSON = new JSONObject(TopScoreJSON.toString());
                                                                        TopScoreJSON = new JSONObject();
                                                                        TopScoreJSON.put(PreferenceName, PreferenceScore);
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                                else if (PreferenceScore > SecondScore) {
                                                                    ThirdScore = SecondScore;
                                                                    SecondScore = PreferenceScore;
                                                                    try {
                                                                        ThirdScoreJSON = new JSONObject(SecondScoreJSON.toString());
                                                                        SecondScoreJSON = new JSONObject();
                                                                        SecondScoreJSON.put(PreferenceName, PreferenceScore);
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                                else {
                                                                    ThirdScore = PreferenceScore;
                                                                    try {
                                                                        ThirdScoreJSON = new JSONObject();
                                                                        ThirdScoreJSON.put(PreferenceName, PreferenceScore);
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }
                                                            //  Check if FourthScore does not have score
                                                            else if (FourthScore == 0) {

                                                                //  Compare with TopScore, SecondScore, ThirdScore
                                                                if (PreferenceScore > TopScore) {
                                                                    FourthScore = ThirdScore;
                                                                    ThirdScore = SecondScore;
                                                                    SecondScore = TopScore;
                                                                    TopScore = PreferenceScore;
                                                                    try {
                                                                        FourthScoreJSON = new JSONObject(ThirdScoreJSON.toString());
                                                                        ThirdScoreJSON = new JSONObject(SecondScoreJSON.toString());
                                                                        SecondScoreJSON = new JSONObject(TopScoreJSON.toString());
                                                                        TopScoreJSON = new JSONObject();
                                                                        TopScoreJSON.put(PreferenceName, PreferenceScore);
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                } else if (PreferenceScore > SecondScore) {
                                                                    FourthScore = ThirdScore;
                                                                    ThirdScore = SecondScore;
                                                                    SecondScore = PreferenceScore;
                                                                    try {
                                                                        FourthScoreJSON = new JSONObject(ThirdScoreJSON.toString());
                                                                        ThirdScoreJSON = new JSONObject(SecondScoreJSON.toString());
                                                                        SecondScoreJSON = new JSONObject();
                                                                        SecondScoreJSON.put(PreferenceName, PreferenceScore);
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                } else if (PreferenceScore > ThirdScore) {
                                                                    FourthScore = ThirdScore;
                                                                    ThirdScore = PreferenceScore;
                                                                    try {
                                                                        FourthScoreJSON = new JSONObject(ThirdScoreJSON.toString());
                                                                        ThirdScoreJSON = new JSONObject();
                                                                        ThirdScoreJSON.put(PreferenceName, PreferenceScore);
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                } else {
                                                                    FourthScore = PreferenceScore;
                                                                    try {
                                                                        FourthScoreJSON = new JSONObject();
                                                                        FourthScoreJSON.put(PreferenceName, PreferenceScore);
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else {  //  After the first four item

                                                            //  Compare with TopScore
                                                            if (PreferenceScore > TopScore) {
                                                                FourthScore = ThirdScore;
                                                                ThirdScore = SecondScore;
                                                                SecondScore = TopScore;
                                                                TopScore = PreferenceScore;
                                                                try {
                                                                    FourthScoreJSON = new JSONObject(ThirdScoreJSON.toString());
                                                                    ThirdScoreJSON = new JSONObject(SecondScoreJSON.toString());
                                                                    SecondScoreJSON = new JSONObject(TopScoreJSON.toString());
                                                                    TopScoreJSON = new JSONObject();
                                                                    TopScoreJSON.put(PreferenceName, PreferenceScore);
                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                            //  Compare with SecondScore
                                                            else if (PreferenceScore > SecondScore) {
                                                                FourthScore = ThirdScore;
                                                                ThirdScore = SecondScore;
                                                                SecondScore = PreferenceScore;
                                                                try {
                                                                    FourthScoreJSON = new JSONObject(ThirdScoreJSON.toString());
                                                                    ThirdScoreJSON = new JSONObject(SecondScoreJSON.toString());
                                                                    SecondScoreJSON = new JSONObject();
                                                                    SecondScoreJSON.put(PreferenceName, PreferenceScore);
                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                            //  Compare with ThirdScore
                                                            else if (PreferenceScore > ThirdScore) {
                                                                FourthScore = ThirdScore;
                                                                ThirdScore = PreferenceScore;
                                                                try {
                                                                    FourthScoreJSON = new JSONObject(ThirdScoreJSON.toString());
                                                                    ThirdScoreJSON = new JSONObject();
                                                                    ThirdScoreJSON.put(PreferenceName, PreferenceScore);
                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                            //  Compare with FourthScore
                                                            else if (PreferenceScore > FourthScore) {
                                                                FourthScore = PreferenceScore;
                                                                try {
                                                                    FourthScoreJSON = new JSONObject();
                                                                    FourthScoreJSON.put(PreferenceName, PreferenceScore);
                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                //  If reach end of item in the collection of preference
                                                if (i == (PreferenceCollections.size() - 1)) {

                                                    try {
                                                        //  Insert to Final JSON
                                                        FinalHighestScores.put(0,TopScoreJSON);
                                                        FinalHighestScores.put(1,SecondScoreJSON);
                                                        FinalHighestScores.put(2,ThirdScoreJSON);
                                                        FinalHighestScores.put(3,FourthScoreJSON);

                                                        Log.d("Final",""+FinalHighestScores);

                                                        splash_screen.setVisibility(View.GONE);
                                                        mToolbar.setVisibility(View.VISIBLE);
                                                        bottomLayout.setVisibility(View.VISIBLE);
                                                        mScrollView.setVisibility(View.VISIBLE);

                                                        //  Move to Recommendation Screen
                                                        Intent recommendationScreen = new Intent(getApplicationContext(), RecommendationResultActivity.class);
                                                        recommendationScreen.putExtra("ScoreResult", FinalHighestScores.toString());
                                                        startActivity(recommendationScreen);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                        Toast.makeText(GroupChatActivity.this, "Problem recommending. Pls try again later.", Toast.LENGTH_LONG).show();
                                                    }
                                                }

                                            }

                                            try {
                                                FinalPreferenceScore.put("Final Preference Score", EachPreferenceScore);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            Log.d("Preference List", ": " + PreferenceList);
                                            Log.d("Preference Score", ": " + FinalPreferenceScore);
                                            Log.d("4 Highest Group Preference Score", ": " + FinalHighestScores);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private <T> List<T> twoDArrayToList(T[][] twoDArray) {
        List<T> list = new ArrayList<T>();
        for (T[] array : twoDArray) {
            list.addAll(Arrays.asList(array));
        }
        return list;
    }
}
