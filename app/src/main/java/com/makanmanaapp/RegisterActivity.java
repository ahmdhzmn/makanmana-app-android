package com.makanmanaapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static java.lang.Integer.parseInt;

public class RegisterActivity extends AppCompatActivity {

    DatabaseHelper db;

    TextInputLayout txtInputEmail, txtInputUsername, txtInputProfileName, txtInputPassword, txtInputCfmPassword;
    EditText txtUsername, txtProfileName, txtEmail, txtPassword, txtCfmPassword;
    Button buttonRegister;
    Boolean usernameIsExist = false;

    ProgressBar spinner;

    private FirebaseAuth mAuth;
    private DatabaseReference RootRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        db = new DatabaseHelper(this);

        mAuth = FirebaseAuth.getInstance();
        RootRef = FirebaseDatabase.getInstance().getReference();
        //  RootRef = https://makanmanaapp-1571315768087.firebaseio.com
        //  Log.d("mylog", "RootRef: " + RootRef);

        txtInputEmail = findViewById(R.id.txt_input_email);
        txtInputUsername = findViewById(R.id.txt_input_username);
        txtInputProfileName = findViewById(R.id.txt_input_profilename);
        txtInputPassword = findViewById(R.id.txt_input_password);
        txtInputCfmPassword = findViewById(R.id.txt_input_confirm_password);

        spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

//        String emailInput = txtInputEmail.getEditText().getText().toString().trim();
//        if (emailInput.isEmpty()) {
//            txtInputEmail.setError("Field can't be empty");
//        } else {
//            txtInputEmail.setError(null);
//            txtInputEmail.setErrorEnabled(false);
//        }
//        String usernameInput = txtInputUsername.getEditText().getText().toString().trim();
//        if (usernameInput.isEmpty()) {
//            txtInputUsername.setError("Field can't be empty");
//        } else {
//            txtInputUsername.setError(null);
//            txtInputUsername.setErrorEnabled(false);
//        }
//        String profilenameInput = txtInputProfileName.getEditText().getText().toString().trim();
//        if (profilenameInput.isEmpty()) {
//            txtInputProfileName.setError("Field can't be empty");
//        } else {
//            txtInputProfileName.setError(null);
//            txtInputProfileName.setErrorEnabled(false);
//        }
//        String passwordInput = txtInputPassword.getEditText().getText().toString().trim();
//        if (passwordInput.isEmpty()) {
//            txtInputPassword.setError("Field can't be empty");
//        } else {
//            txtInputPassword.setError(null);
//            txtInputPassword.setErrorEnabled(false);
//        }
//        String cfmpasswordInput = txtInputCfmPassword.getEditText().getText().toString().trim();
//        if (cfmpasswordInput.isEmpty()) {
//            txtInputCfmPassword.setError("Field can't be empty");
//        } else {
//            txtInputCfmPassword.setError(null);
//            txtInputCfmPassword.setErrorEnabled(false);
//        }

        txtUsername = findViewById(R.id.txt_username);
        txtProfileName = findViewById(R.id.txt_profilename);
        txtEmail = findViewById(R.id.txt_email);
        txtPassword = findViewById(R.id.txt_password);
        txtCfmPassword = findViewById(R.id.txt_cfm_password);
        buttonRegister = findViewById(R.id.btn_register);

//        ((EditText)findViewById(R.id.txt_username)).setOnEditorActionListener(
//                new EditText.OnEditorActionListener() {
//                    @Override
//                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
//                                actionId == EditorInfo.IME_ACTION_DONE ||
//                                event != null &&
//                                        event.getAction() == KeyEvent.ACTION_DOWN &&
//                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
//                            if (event == null || !event.isShiftPressed()) {
//                                // the user is done typing.
//                                final String usernameEntered = txtUsername.getText().toString();
//                                RootRef.child("Users").orderByChild("username").equalTo(usernameEntered).addListenerForSingleValueEvent(
//                                        new ValueEventListener() {
//                                            @Override
//                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                                Log.d()
//                                                if (dataSnapshot.exists()) {
//                                                    Toast.makeText(getApplicationContext(), "Username already exists. Please try other username.", Toast.LENGTH_SHORT).show();
//                                                    usernameCheck = false;
//                                                }
//                                                else {
//                                                    usernameCheck = true;
//                                                }
//                                            }
//
//                                            @Override
//                                            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                            }
//                                        }
//                                );
//                                return true; // consume.
//                            }
//                        }
//                        return false; // pass on to other listeners.
//                    }
//                }
//        );

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                spinner.setVisibility(View.VISIBLE);

                final String username = txtUsername.getText().toString().trim();
                final String profilename = txtProfileName.getText().toString().trim();
                final String email = txtEmail.getText().toString().trim();
                final String password = txtPassword.getText().toString().trim();
                final String cfmpassword = txtCfmPassword.getText().toString().trim();
                final int usertype = 1;
                final int requestedrecommendation = 0;



                if (TextUtils.isEmpty(username) || TextUtils.isEmpty(profilename) || TextUtils.isEmpty(email) ||
                        TextUtils.isEmpty(password) || TextUtils.isEmpty(cfmpassword) ) {
                    spinner.setVisibility(View.GONE);
                    Toast.makeText(RegisterActivity.this,"Please fill in all credentials!", Toast.LENGTH_LONG).show();
                } else {
                    RootRef.child("Users").orderByChild("username").equalTo(username).addListenerForSingleValueEvent(
                            new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    Log.d("mylog", "DataSnapshot (username): " + dataSnapshot);
                                    if (dataSnapshot.getChildrenCount() > 0) {
                                        spinner.setVisibility(View.GONE);
                                        Toast.makeText(getApplicationContext(), "Username already exists. Please try other username.", Toast.LENGTH_LONG).show();
                                    }
                                    else {
                                        if (password.equals(cfmpassword)) {
                                            mAuth.createUserWithEmailAndPassword(email, password)
                                                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                                            if (task.isSuccessful()) {

                                                                User user = new User(
                                                                        usertype,
                                                                        requestedrecommendation,
                                                                        username,
                                                                        profilename,
                                                                        email
                                                                );

                                                                //  Other method to insert value:
                                                                //  HashMap<String,String,String,String> profileMap = new HashMap<>();
                                                                //  profileMap.put("uid", currentUserId);
                                                                //  profileMap.put("usertype", "1");
                                                                //  profileMap.put("username", username);
                                                                //  profileMap.put("profilename", profilename);
                                                                //  profileMap.put("email", email);
                                                                //  Then masuk mcm biasa RootRef.....setValue(profileMap).....addOnComplete....

                                                                //  Insert into firebase database called "Users" using unique user id
                                                                //  that is generated while creating user using FirebaseAuth
                                                                RootRef.child("Users")
                                                                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                                        .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                        if(task.isSuccessful()) {
                                                                            Toast.makeText(RegisterActivity.this,"Account created!", Toast.LENGTH_SHORT).show();
                                                                            Intent loginScreen = new Intent(RegisterActivity.this, LoginActivity.class);
                                                                            loginScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                            spinner.setVisibility(View.GONE);
                                                                            startActivity(loginScreen);
                                                                            finish();
                                                                        }
                                                                        else {
                                                                            System.out.println(task.getException());
                                                                            spinner.setVisibility(View.GONE);
                                                                            Toast.makeText(RegisterActivity.this, "Registration Error! Please try again later.", Toast.LENGTH_LONG).show();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            else {
                                                                String message = task.getException().toString();
                                                                spinner.setVisibility(View.GONE);
                                                                Toast.makeText(RegisterActivity.this, "Error: " + message, Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    });

                                        }
                                        else{
                                            spinner.setVisibility(View.GONE);
                                            Toast.makeText(RegisterActivity.this,"Password mismatch!", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            }
                    );


                }


//                if(username.equals("")||fname.equals("")||lname.equals("")||pwd.equals("")||cfmpassword.equals("")||strPhone.equals("")) {
//                    Toast.makeText(RegisterActivity.this,"Empty Field! Pls fill in all details", Toast.LENGTH_SHORT).show();
//                }
//                else
//                {
//                    if(pwd.equals(cfmpassword)) {
//
//                        Boolean checkUsername = db.checkUsername(username);
//
//                        if(checkUsername == true) {
//                            long val = db.addUser(username,fname,lname,pwd,phone);
//
//                            if(val > 0) {
//                                Toast.makeText(RegisterActivity.this,"Successfully Registered!", Toast.LENGTH_SHORT).show();
//                                Intent moveToLogin = new Intent(RegisterActivity.this, TestLoginActivity.class);
//                                startActivity(moveToLogin);
//                            }
//                            else {
//                                Toast.makeText(RegisterActivity.this, "Registration Error!", Toast.LENGTH_SHORT).show();
//                            }
//
//                        }
//                        else {
//                            Toast.makeText(RegisterActivity.this,"Username Already Exist! Try a different username", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    else{
//                        Toast.makeText(RegisterActivity.this,"Password mismatch!", Toast.LENGTH_SHORT).show();
//                    }
//                }
            }
        });
    }
}
