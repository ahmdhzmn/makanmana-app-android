package com.makanmanaapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;

    private FirebaseUser currentUser;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference RootRef;

    Location currentLocation;
    LocationManager locationManager;

    FusedLocationProviderClient fusedLocationProviderClient;
    private static final int REQUEST_CODE = 101;

    private String currentUserId;

    TextView displayUserEmail, displayUserProfileName, displayUserLocation, displayUserCoordinates;

    String myLocation, myCoordinates;

    //  private String userId, email, username, profilename, usertype;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        RootRef = FirebaseDatabase.getInstance().getReference();


        if(currentUser != null) {
            RootRef.child("Users").child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (dataSnapshot.exists()) {

                        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("userId", mAuth.getCurrentUser().getUid());
                        editor.putString("userName", dataSnapshot.child("username").getValue().toString());
                        editor.putString("profileName", dataSnapshot.child("profilename").getValue().toString());
                        editor.putString("userEmail", dataSnapshot.child("email").getValue().toString());
                        editor.putString("userNumberOfRecommend", dataSnapshot.child("requestedrecommendation").getValue().toString());
                        editor.putString("userType", dataSnapshot.child("usertype").getValue().toString());
                        editor.putBoolean("isLogin", true);
                        editor.apply();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }


        displayUserEmail = findViewById(R.id.home_userEmail);
        displayUserProfileName = findViewById(R.id.home_profileName);
        displayUserLocation = findViewById(R.id.home_userLocation);
        displayUserCoordinates = findViewById(R.id.home_userCoordinates);


        //  To get User's Location
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fetchLastLocation();



        // Bottom Tab
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        // Starting fragment in this MainActivity
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment = null;

            switch (menuItem.getItemId()) {
                case R.id.nav_home:
                    selectedFragment = new HomeFragment();
                    break;
                case R.id.nav_group_chat:
                    selectedFragment = new GroupChatFragment();
                    break;
                case R.id.nav_profile:
                    selectedFragment = new ProfileFragment();
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();

            return true;
        }
    };

    private void fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_CODE);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location != null) {
                    currentLocation = location;
                    //  Toast.makeText(getApplicationContext(),currentLocation.getLatitude() + " " + currentLocation.getLongitude(), Toast.LENGTH_LONG).show();
                }

                LatLng myCoordinates = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());

                // Call method to display user location
                getCityName(myCoordinates);

                //  Call method to display user coordinates
                String userCoordinates = myCoordinates.toString();
                //  passUserCoordinates(userCoordinates);

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE:
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLastLocation();
                }
                break;
        }
    }

    private void getCityName(LatLng userCoordinates) {
        String myCity = "";
        String myState = "";
        String myLatitude = "";
        String myLongitude = "";
        myLocation = "";
        myCoordinates = "";
        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(userCoordinates.latitude, userCoordinates.longitude, 1);
            String address = addresses.get(0).getAddressLine(0);
            myCity = addresses.get(0).getLocality();
            myState = addresses.get(0).getAdminArea();
            myLatitude = String.format ("%.7f", addresses.get(0).getLatitude());
            myLongitude = String.format ("%.7f", addresses.get(0).getLongitude());
            //myFullAddress = addresses.get(0).getAddressLine(0);
            Log.d("mylog", "Complete Address: " + addresses.toString());
            Log.d("mylog", "Address: " + address);
        } catch (IOException e) {
            e.printStackTrace();
        }
        myLocation = myCity + ", " + myState;
        myCoordinates = "(" + myLatitude + ", " + myLongitude + ")";
        // return myLocation;

        // passUserLocation(myLocation);

        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("userLocation", myLocation);
        editor.putString("userCoordinates", myCoordinates);
        editor.apply();

    }

    @Override
    protected void onStart() {
        super.onStart();

        if(currentUser == null) {
            fetchLastLocation();
            Intent loginScreen = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(loginScreen);
        }
        else {
//            if (sharedpreferences.contains("userLocation") && sharedpreferences.contains("userCoordinates")) {
//                displayUserLocation.setText(sharedpreferences.getString("userLocation",""));
//                displayUserCoordinates.setText(sharedpreferences.getString("userCoordinates",""));
//            }

            currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();

            //  Check for userId is exist in realtime database
            RootRef.child("Users").orderByKey().equalTo(currentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getChildrenCount() < 1) {
                        Intent firstTimeLoginScreen = new Intent(MainActivity.this, NewFacebookUserActivity.class);
                        firstTimeLoginScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(firstTimeLoginScreen);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


            sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
            String userType = sharedpreferences.getString("userType", "");

            if (userType.equals("1")) {
                //  Check if user have set preferences
                RootRef.child("Users").child(currentUserId).child("Preferences").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences.edit();

                            if (dataSnapshot.hasChild("mainpreference")) {
                                editor.putString("mainPreference", dataSnapshot.child("mainpreference").getValue().toString());
                                editor.apply();
                            } else {
                                editor.putString("mainPreference", "");
                                editor.apply();
                            }

                            if (dataSnapshot.hasChild("secondpreference")) {
                                editor.putString("secondPreference", dataSnapshot.child("secondpreference").getValue().toString());
                                editor.apply();
                            } else {
                                editor.putString("secondPreference", "");
                                editor.apply();
                            }

                            if (dataSnapshot.hasChild("thirdpreference")) {
                                editor.putString("thirdPreference", dataSnapshot.child("thirdpreference").getValue().toString());
                                editor.apply();
                            } else {
                                editor.putString("thirdPreference", "");
                                editor.apply();
                            }

                            if (dataSnapshot.hasChild("fourthpreference")) {
                                editor.putString("fourthPreference", dataSnapshot.child("fourthpreference").getValue().toString());
                                editor.apply();
                            } else {
                                editor.putString("fourthPreference", "");
                                editor.apply();
                            }

                            if (!(dataSnapshot.hasChild("mainpreference") && dataSnapshot.hasChild("secondpreference")
                                    && dataSnapshot.hasChild("thirdpreference") && dataSnapshot.hasChild("fourthpreference"))) {
                                Toast.makeText(getApplicationContext(),"Please set all preferences", Toast.LENGTH_LONG).show();

                                Intent SetPreferencesScreen = new Intent(MainActivity.this, SetPreferenceActivity.class);
                                SetPreferencesScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(SetPreferencesScreen);
                            }

                        } else {
                            sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences.edit();

                            editor.putString("mainPreference", "");
                            editor.putString("secondPreference", "");
                            editor.putString("thirdPreference", "");
                            editor.putString("fourthPreference", "");
                            editor.putInt("mainPreferenceValuePosition", 0);
                            editor.putInt("secondPreferenceValuePosition", 0);
                            editor.putInt("thirdPreferenceValuePosition", 0);
                            editor.putInt("fourthPreferenceValuePosition", 0);

                            editor.apply();

                            Toast.makeText(getApplicationContext(),"Please set your preferences to continue using this app", Toast.LENGTH_LONG).show();
                            Intent SetPreferencesScreen = new Intent(MainActivity.this, SetPreferenceActivity.class);
                            SetPreferencesScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(SetPreferencesScreen);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        fetchLastLocation();


    }

    //    public String passUserCoordinates(String myCoordinates) {
//        return myCoordinates;
//    }

//    public String passUserLocation(String myLocation) {
//        return myLocation;
//    }

}
