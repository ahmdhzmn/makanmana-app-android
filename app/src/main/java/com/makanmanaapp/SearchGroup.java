package com.makanmanaapp;

public class SearchGroup {
    private String groupName, groupCreatedBy;

    public SearchGroup() {
    }

    public SearchGroup(String groupName, String groupCreatedBy) {
        this.groupName = groupName;
        this.groupCreatedBy = groupCreatedBy;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupCreatedBy() {
        return groupCreatedBy;
    }

    public void setGroupCreatedBy(String groupCreatedBy) {
        this.groupCreatedBy = groupCreatedBy;
    }
}
