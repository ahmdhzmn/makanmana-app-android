package com.makanmanaapp;

public class User {
    public int usertype, requestedrecommendation; //     1 - normal user     2 - fb user
    public String username, profilename, email;

    public User() {

    }

    public User(int usertype, int requestedrecommendation, String username, String profilename, String email) {
        this.usertype = usertype;
        this.requestedrecommendation = requestedrecommendation;
        this.username = username;
        this.profilename = profilename;
        this.email = email;
    }


}
