package com.makanmanaapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FindGroupsActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ImageButton searchButton;
    private EditText groupSearchInput;
    private TextView searchResultDisplay;

    private RecyclerView GroupSearchRecylerList;
    private RecyclerView AllGroupsRecyclerList;

    private ProgressBar spinner;
    private ProgressBar spinner2;

    private RelativeLayout mainLayout;

    DatabaseReference RootRef = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_groups);

        mainLayout = findViewById(R.id.search_group_main_layout);

        mToolbar = findViewById(R.id.toolbar_find_group);
        groupSearchInput = findViewById(R.id.group_search_input);
        searchButton = findViewById(R.id.group_search_button);
        searchResultDisplay = findViewById(R.id.search_result_text);
        searchResultDisplay.setVisibility(View.INVISIBLE);

        spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);
        spinner2 = findViewById(R.id.progressBarExistingGroup);
        spinner2.setVisibility(View.VISIBLE);

        setSupportActionBar(mToolbar);

        GroupSearchRecylerList = findViewById(R.id.group_search_result_list);
        GroupSearchRecylerList.setHasFixedSize(true);
        GroupSearchRecylerList.setLayoutManager(new LinearLayoutManager(FindGroupsActivity.this));

        AllGroupsRecyclerList = findViewById(R.id.all_group_list);
        AllGroupsRecyclerList.setHasFixedSize(true);
        AllGroupsRecyclerList.setLayoutManager(new LinearLayoutManager(FindGroupsActivity.this));

        DisplayAllGroup();

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchResultDisplay.setVisibility(View.GONE);
                spinner.setVisibility(View.VISIBLE);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);

                final String searchGroupName = groupSearchInput.getText().toString().trim();

                if (TextUtils.isEmpty(searchGroupName)) {
                    searchResultDisplay.setVisibility(View.INVISIBLE);
                    GroupSearchRecylerList.setVisibility(View.GONE);
                    spinner.setVisibility(View.GONE);
                    openDialog();

                } else {

                    SearchGroup(searchGroupName);

                    //  TRY THIS NEW CONCEPT PLS!!
                    //  SearchGroup(searchGroupName.toLowerCase());
                    //  Pastu dlm firebase Groups ada value sort_group_name letak nama group in lower case(waktu register group tambah children sort_group_name)
                    //  Then search based on tu sahaja!!

                    spinner.setVisibility(View.GONE);
                    searchResultDisplay.setText("Search Result:");
                    GroupSearchRecylerList.setVisibility(View.VISIBLE);
//                    RootRef.child("Groups").orderByKey().equalTo(searchGroupName).addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                            if(dataSnapshot.exists()) {
//                                SearchGroup(searchGroupName);
//                                spinner.setVisibility(View.GONE);
//                                searchResultDisplay.setText("Search Result:");
//                                GroupSearchRecylerList.setVisibility(View.VISIBLE);
//                            }
//                            else {
//                                spinner.setVisibility(View.GONE);
//                                searchResultDisplay.setText("Search Result:  Group not found");
//                                searchResultDisplay.setVisibility(View.VISIBLE);
//                                GroupSearchRecylerList.setVisibility(View.GONE);
//                            }
//                        }
//
//                        @Override
//                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                        }
//                    });
                }


            }
        });
    }

    private void SearchGroup(String searchGroupName) {

        searchResultDisplay.setVisibility(View.VISIBLE);

        //  Query asal
        //  RootRef.child("Groups").orderByKey().equalTo(searchGroupName)
        FirebaseRecyclerOptions<SearchGroup> options = new FirebaseRecyclerOptions.Builder<SearchGroup>()
                .setQuery(RootRef.child("Groups").orderByKey().startAt(searchGroupName).endAt(searchGroupName + "\uf8ff"), new SnapshotParser<SearchGroup>() {
                    @NonNull
                    @Override
                    public SearchGroup parseSnapshot(@NonNull DataSnapshot snapshot) {
                        final SearchGroup searchGroup = new SearchGroup();

                        RootRef.child("Groups").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                for (DataSnapshot groupDetailSnapshot: dataSnapshot.getChildren()) {

                                    SearchGroup group = dataSnapshot.getValue(SearchGroup.class);

                                    String groupName = groupDetailSnapshot.getKey().toString();

                                    searchGroup.setGroupName(groupName);
//                                    Log.d("Get Data name", groupName);
//                                    Log.d("Get Data", group.getGroupName());
                                    //  Log.d("mylog", "DataSnapshot (using set): " + dataSnapshot);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        return searchGroup;
                    }
                })
                .build();

        FirebaseRecyclerAdapter<SearchGroup, SearchGroupViewHolder> adapter
                = new FirebaseRecyclerAdapter<SearchGroup, SearchGroupViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final SearchGroupViewHolder holder, int position, @NonNull SearchGroup model) {

                holder.txtGroupName.setText(getRef(position).getKey().toString());

                RootRef.child("Groups").child(getRef(position).getKey().toString()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String retGroupCreatedBy = dataSnapshot.child("groupCreatedBy").getValue().toString();

                        //  Find username of the group creator
                        RootRef.child("Users").child(retGroupCreatedBy).child("profilename").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if(dataSnapshot.exists()) {
                                    holder.txtGroupCreatedBy.setText("Group created by: " + dataSnapshot.getValue().toString());
                                } else {
                                    holder.txtGroupCreatedBy.setText("Group created by: (Account no longer exist)");
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                holder.joinGroupButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        spinner.setVisibility(View.VISIBLE);

                        final String groupName = holder.txtGroupName.getText().toString();

                        RootRef.child("Groups").child(groupName).child("Users")
                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .setValue("").addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()) {

                                    RootRef.child("UserGroups")
                                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                            .child(groupName).setValue("").addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()) {
                                                spinner.setVisibility(View.GONE);
                                                Toast.makeText(FindGroupsActivity.this, "Successfully joined " + groupName + "!", Toast.LENGTH_LONG).show();
                                                finish();
                                            }
                                            else {
                                                spinner.setVisibility(View.GONE);
                                            }
                                        }
                                    });
                                }
                                else {
                                    spinner.setVisibility(View.GONE);
                                    Toast.makeText(FindGroupsActivity.this, "Error joining group! Pls try again later", Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                    }
                });
            }

            @NonNull
            @Override
            public SearchGroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_search_display_layout, parent, false);
                return new SearchGroupViewHolder(view);
            }
        };

        GroupSearchRecylerList.setAdapter(adapter);
        adapter.startListening();
    }

    public static class SearchGroupViewHolder extends RecyclerView.ViewHolder {

        TextView txtGroupName, txtGroupCreatedBy;
        Button joinGroupButton;

        public SearchGroupViewHolder(@NonNull View itemView) {
            super(itemView);

            txtGroupName = itemView.findViewById(R.id.display_group_name);
            txtGroupCreatedBy = itemView.findViewById(R.id.display_group_createdby);
            joinGroupButton = itemView.findViewById(R.id.join_group_button);
        }
    }

    private void DisplayAllGroup() {

        spinner2.setVisibility(View.VISIBLE);

        FirebaseRecyclerOptions<Group> optionsAllGroup = new FirebaseRecyclerOptions.Builder<Group>()
                .setQuery(RootRef.child("Groups"), new SnapshotParser<Group>() {
                    @NonNull
                    @Override
                    public Group parseSnapshot(@NonNull DataSnapshot snapshot) {
                        final Group group = new Group();

                        RootRef.child("Groups").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                for (DataSnapshot groupDetailSnapshot: dataSnapshot.getChildren()) {

                                    Group group = dataSnapshot.getValue(Group.class);

                                    String groupName = groupDetailSnapshot.getKey().toString();

                                    group.setGroupName(groupName);
//                                    Log.d("Get Data name", groupName);
//                                    Log.d("Get Data", group.getGroupName());
                                    //  Log.d("mylog", "DataSnapshot (using set): " + dataSnapshot);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        return group;
                    }
                })
                .build();

        FirebaseRecyclerAdapter<Group, AllGroupViewHolder> adapterAllGroup
                = new FirebaseRecyclerAdapter<Group, AllGroupViewHolder>(optionsAllGroup) {
            @Override
            protected void onBindViewHolder(@NonNull final AllGroupViewHolder holder, int position, @NonNull Group model) {

                spinner2.setVisibility(View.GONE);

                holder.txtAllGroupName.setText(getRef(position).getKey().toString());

                RootRef.child("Groups").child(getRef(position).getKey().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String retGroupCreatedBy = dataSnapshot.child("groupCreatedBy").getValue().toString();

                        //  Find username of the group creator
                        RootRef.child("Users").child(retGroupCreatedBy).child("profilename").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if(dataSnapshot.exists()) {
                                    holder.txtAllGroupCreatedBy.setText("Group created by: " + dataSnapshot.getValue().toString());
                                } else {
                                    holder.txtAllGroupCreatedBy.setText("Group created by: (Account no longer exist)");
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @NonNull
            @Override
            public AllGroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_group_display_layout, parent, false);
                return new AllGroupViewHolder(view);
            }
        };

        AllGroupsRecyclerList.setAdapter(adapterAllGroup);
        adapterAllGroup.startListening();
    }

    public static class AllGroupViewHolder extends RecyclerView.ViewHolder {

        TextView txtAllGroupName, txtAllGroupCreatedBy;

        public AllGroupViewHolder(@NonNull View itemView) {
            super(itemView);

            txtAllGroupName = itemView.findViewById(R.id.display_all_group_name);
            txtAllGroupCreatedBy = itemView.findViewById(R.id.display_all_group_createdby);
        }
    }


    private void openDialog() {
        GroupSearchDisplayDialog exampleDialog = new GroupSearchDisplayDialog();
        exampleDialog.show(getSupportFragmentManager(), "group search error dialog");
    }

}
