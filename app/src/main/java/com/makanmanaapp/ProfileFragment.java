package com.makanmanaapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileFragment extends Fragment {

    SharedPreferences sharedpreferences;

    String id, email, username, profilename, imageUrl, userLocation, userType, numberOfRecommend;
    TextView txtUsername, txtEmail, txtProfilename, txtBigProfilename, txtUserLocation, txtUserType, txtNumberOfGroups, txtNumberOfRecommendation;

    View view;

    Menu option_menu;

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    DatabaseReference RootRef = FirebaseDatabase.getInstance().getReference();

    long countGroup;

    String userId;

    @Nullable
    @Override
    //test
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar_profile);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        sharedpreferences = getActivity().getSharedPreferences("userPref", Context.MODE_PRIVATE);

        id = sharedpreferences.getString("userId", "");
        email = sharedpreferences.getString("userEmail", "");
        username = sharedpreferences.getString("userName", "");
        profilename = sharedpreferences.getString("profileName", "");
        userLocation = sharedpreferences.getString("userLocation", "");

        numberOfRecommend = sharedpreferences.getString("userNumberOfRecommend", "");

        txtUsername = view.findViewById(R.id.txt_profilescreen_username);
        txtEmail = view.findViewById(R.id.txt_profilescreen_email);
        txtProfilename = view.findViewById(R.id.txt_profilescreen_profname);
        txtBigProfilename = view.findViewById(R.id.profilescreen_profname);
        txtUserLocation = view.findViewById(R.id.profilescreen_location);
        txtNumberOfGroups = view.findViewById(R.id.number_of_groups);
        txtNumberOfRecommendation = view.findViewById(R.id.number_of_recommendation);

        validateUserType();

        txtUsername.setText(username);
        txtEmail.setText(email);
        txtProfilename.setText(profilename);
        txtBigProfilename.setText(profilename);
        txtUserLocation.setText(userLocation);
        txtNumberOfRecommendation.setText(numberOfRecommend);

        // Count user groups
        countUserGroups();

        return view;
    }

    private void validateUserType() {

        sharedpreferences = getActivity().getSharedPreferences("userPref", Context.MODE_PRIVATE);

        userType = sharedpreferences.getString("userType", "");
        txtUserType = view.findViewById(R.id.txt_profilescreen_usertype);

        if (userType.equals("1")) {
            txtUserType.setText("Guest User");
        }
        else
            txtUserType.setText("Facebook User");
    }

    private void countUserGroups() {

        RootRef.child("UserGroups").child(currentUser.getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        countGroup = dataSnapshot.getChildrenCount();
                        txtNumberOfGroups.setText(Long.toString(countGroup));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.option_toolbar_profile, menu);

        option_menu = menu;
        sharedpreferences = getActivity().getSharedPreferences("userPref", Context.MODE_PRIVATE);
        userType = sharedpreferences.getString("userType", "");
        if (userType.equals("2")) {
            option_menu.findItem(R.id.set_preference_option).setVisible(false);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch(item.getItemId()) {
            case R.id.restaurant_preference_option:
                Intent setPreferenceScreen = new Intent(getActivity(), SetPreferenceActivity.class);
                startActivity(setPreferenceScreen);
                break;
            case R.id.edit_profile_option:
                Intent profileScreen = new Intent(getActivity(), EditProfileActivity.class);
                startActivity(profileScreen);
                break;
            case R.id.delete_profile_option:
                userId = currentUser.getUid().toString();
                //Toast.makeText(getActivity(),"Delete profile..", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Delete Profile");
                builder.setMessage("Are you sure you want to delete your profile?");

                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        //  Delete user authentication
                        currentUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    FirebaseAuth.getInstance().signOut();
                                    LoginManager.getInstance().logOut();
                                    currentUser = null;
                                    Toast.makeText(getActivity(),"Account Deleted", Toast.LENGTH_LONG).show();

                                    //  Delete user in table Groups
                                    RootRef.child("UserGroups").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                            for (DataSnapshot usergroupsSnaphot: dataSnapshot.getChildren()) {
                                                String groupName = usergroupsSnaphot.getKey().toString();

                                                RootRef.child("Groups").child(groupName).child("Users").child(userId).removeValue();
                                            }

                                            //  Delete user in table UserGroups
                                            RootRef.child("UserGroups").child(userId).removeValue();

                                            //  Delete user in table Users
                                            RootRef.child("Users").child(userId).removeValue();

                                            Intent loginScreen = new Intent(getActivity(), LoginActivity.class);
                                            loginScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(loginScreen);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                                else {
                                    //  Toast.makeText(getActivity(),task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                    Toast.makeText(getActivity(), "Error Deleting Account. Please try again later", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                builder.show();

                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
