package com.makanmanaapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String TAG="MAKANMANA";
    public static final String DATABASE_NAME="MakanManaApp.db";
    public static final String TABLE_NAME="user";
    public static final String COL_1="id";
    public static final String COL_2="username";
    public static final String COL_3="fname";
    public static final String COL_4="lname";
    public static final String COL_5="password";
    public static final String COL_6="phone";
    private String username;
    private String password;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, fname TEXT, lname TEXT, password TEXT, phone INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public long addUser(String username, String fname, String lname, String password, int phone) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("username",username);
        contentValues.put("fname",fname);
        contentValues.put("lname",lname);
        contentValues.put("password", password);
        contentValues.put("phone", phone);
        long res = db.insert("user", null, contentValues);
        db.close();
        return res;
    }

    public Boolean checkUser(String username, String password) {

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM user WHERE username=? and password=?", new String[]{username, password});
        int count = cursor.getCount();
        cursor.close();
        db.close();

        if(cursor.getCount()>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Boolean checkUsername(String username) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM user where username=?", new String[]{username});
        if(cursor.getCount()>0) {
            return false;
        }
        else {
            return true;
        }
    }

    public Cursor alldata() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM user", null);
        return cursor;
    }

    public Boolean update(int id, String username, int phone) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        Log.e(TAG, username+""+phone);
        contentValues.put("username", username);
        contentValues.put("phone", phone);
        db.update("user", contentValues, "id = ?", new String[]{String.valueOf(id)});
        return true;
    }

    public Boolean delete(Integer id) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("user", "id = ? ", new String[]{String.valueOf(id)});
        return true;
    }
}
