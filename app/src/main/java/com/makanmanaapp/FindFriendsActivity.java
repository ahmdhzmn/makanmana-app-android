package com.makanmanaapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FindFriendsActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private RecyclerView FriendRecyclerList;
    private DatabaseReference UsersRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);

        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");


        FriendRecyclerList = findViewById(R.id.find_friends_list);
        FriendRecyclerList.setLayoutManager(new LinearLayoutManager(FindFriendsActivity.this));
        mToolbar = findViewById(R.id.toolbar_view_users);

        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseRecyclerOptions<UserDisplay> options =
                new FirebaseRecyclerOptions.Builder<UserDisplay>()
                .setQuery(UsersRef, UserDisplay.class)
                .build();

        FirebaseRecyclerAdapter<UserDisplay, FindFriendViewHolder> adapter =
                new FirebaseRecyclerAdapter<UserDisplay, FindFriendViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull FindFriendViewHolder holder, int position, @NonNull UserDisplay model) {
                        holder.userProfileName.setText("Profile Name : " + model.getProfilename());
                        holder.userUsername.setText("Username : " + model.getUsername());
                    }

                    @NonNull
                    @Override
                    public FindFriendViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_display_layout, parent, false);
                        return new FindFriendViewHolder(view);
                    }
                };
        FriendRecyclerList.setAdapter(adapter);
        adapter.startListening();
    }

    public static class FindFriendViewHolder extends RecyclerView.ViewHolder {

        TextView userProfileName, userUsername;

        public FindFriendViewHolder(@NonNull View itemView) {
            super(itemView);

            userProfileName = itemView.findViewById(R.id.display_user_profilename);
            userUsername = itemView.findViewById(R.id.display_user_username);
        }
    }
}
