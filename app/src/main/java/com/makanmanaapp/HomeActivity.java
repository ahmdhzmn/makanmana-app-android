package com.makanmanaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import static java.lang.Integer.parseInt;

public class HomeActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;

    int userId;

    Button mButtonDelete;
    Button mButtonUpdate;
    TextView mTextViewUsername;
    TextView mTextViewPhone;
    TextView mTitle;
    Button buttonLogout;

    DatabaseHelper db;

    String editedUsername;
    String editedPhone;
    int editedPhoneInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

//        db = new DatabaseHelper(this);
//
//        mTitle = (TextView) findViewById(R.id.textView);
//        mButtonDelete = (Button) findViewById(R.id.deleteButton);
//        mButtonUpdate = (Button) findViewById(R.id.updateButton);
//        mTextViewUsername = (TextView) findViewById(R.id.textViewUsername);
//        mTextViewPhone = (TextView) findViewById(R.id.textViewPhone);
//        buttonLogout = findViewById(R.id.button_logout);
//
//        sharedpreferences = getSharedPreferences("mypref", Context.MODE_PRIVATE);
//
//        userId = sharedpreferences.getInt("userId", 0);
//
////        Toast.makeText(HomeActivity.this,"User: id:" + userId, Toast.LENGTH_SHORT).show();
//
//        mTitle.setText("Welcome, " + sharedpreferences.getString("userLname", ""));
//
//
//        if (sharedpreferences.contains("username")) {
//            mTextViewUsername.setText(sharedpreferences.getString("username", ""));
//        }
//
//        if (sharedpreferences.contains("userPhone")) {
//            mTextViewPhone.setText(Integer.toString(sharedpreferences.getInt("userPhone", 0)));
//        }
//
//        mButtonUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                editedUsername = mTextViewUsername.getText().toString();
//                editedPhone = mTextViewPhone.getText().toString();
//                editedPhoneInt = Integer.parseInt(editedPhone);
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
//
//                builder.setTitle("Update Profile");
//                builder.setMessage("Are you sure?");
//                builder.setCancelable(true);
//                builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//
////                      Toast.makeText(HomeActivity.this,"User: " + editedUsername, Toast.LENGTH_SHORT).show();
////                      Toast.makeText(HomeActivity.this,"Phone: " + editedPhoneInt, Toast.LENGTH_SHORT).show();
//
//                        Boolean res = db.update(userId, editedUsername, editedPhoneInt);
//
//                        if (res) {
//                            Toast.makeText(HomeActivity.this,"Update successful", Toast.LENGTH_SHORT).show();
//
//                            sharedpreferences = getSharedPreferences("mypref", Context.MODE_PRIVATE);
//                            SharedPreferences.Editor editor = sharedpreferences.edit();
//                            editor.putString("username", editedUsername);
//                            editor.putInt("userPhone", editedPhoneInt);
//                            editor.apply();
//                        }
//                        else {
//                            Toast.makeText(HomeActivity.this,"Update Error!", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.cancel();
//                    }
//                });
//
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();
//            }
//        });
//
//        buttonLogout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                LoginManager.getInstance().logOut();
//
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.putString("userId", "");
//                editor.putString("userName", "");
//                editor.putString("userEmail", "");
//                editor.putString("userImageUrl", "");
//                editor.putBoolean("userLogin", false);
//                editor.apply();
//
//                Intent FBLoginScreen = new Intent(HomeActivity.this, TestFBLoginActivity.class);
//                startActivity(FBLoginScreen);
//
//                finish();
//            }
//        });
//
//        mButtonDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
//
//                builder.setTitle("Delete Account");
//                builder.setMessage("Are you sure?");
//                builder.setCancelable(true);
//                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//
//                        Boolean res = db.delete(userId);
//
//                        if (res) {
//                            Toast.makeText(HomeActivity.this,"Account deleted", Toast.LENGTH_SHORT).show();
//
//                            sharedpreferences = getSharedPreferences("mypref", Context.MODE_PRIVATE);
//                            SharedPreferences.Editor editor = sharedpreferences.edit();
//                            editor.putInt("userId", 0);
//                            editor.putString("username", "");
//                            editor.putString("userFname", "");
//                            editor.putString("userLname", "");
//                            editor.putString("userPassword", "");
//                            editor.putInt("userPhone", 0);
//                            editor.apply();
//
//                            Intent moveToLogin = new Intent(HomeActivity.this, TestLoginActivity.class);
//                            startActivity(moveToLogin);
//                        }
//                        else {
//                            Toast.makeText(HomeActivity.this,"Error deleting account", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.cancel();
//                    }
//                });
//
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();
//            }
//        });
    }

//    SharedPreferences sharedpreferences;
//
//    private CircleImageView circleImageView;
//
//    private TextView txtFbId, txtFbName, txtFbEmail;
//    private Button buttonLogout;
//
//    String userId;
//    String userName;
//    String userEmail;
//    String imageUrl;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_test_fblogin);
//
//        FacebookSdk.sdkInitialize(this);
//
//        circleImageView = findViewById(R.id.fb_profile_image);
//        txtFbId = findViewById(R.id.fb_id);
//        txtFbName = findViewById(R.id.fb_name);
//        txtFbEmail = findViewById(R.id.fb_email);
//        buttonLogout = findViewById(R.id.button_logout);
//
//        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
//
//        userId = sharedpreferences.getString("userId", "");
//        userName = sharedpreferences.getString("userName", "");
//        userEmail = sharedpreferences.getString("userEmail", "");
//        imageUrl = sharedpreferences.getString("userImageUrl", "");
//
//        RequestOptions requestOptions = new RequestOptions();
//        requestOptions.dontAnimate();
//
//        txtFbId.setText(userId);
//        txtFbName.setText(userName);
//        txtFbEmail.setText(userEmail);
//        Glide.with(HomeActivity.this).load(imageUrl).into(circleImageView);
//
//        buttonLogout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                LoginManager.getInstance().logOut();
//
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.putString("userId", "");
//                editor.putString("userName", "");
//                editor.putString("userEmail", "");
//                editor.putString("userImageUrl", "");
//                editor.putBoolean("userLogin", false);
//                editor.apply();
//
//                Intent FBLoginScreen = new Intent(HomeActivity.this, TestFBLoginActivity.class);
//                startActivity(FBLoginScreen);
//
//                finish();
//            }
//        });
//
//    }


    @Override
    public void onBackPressed() {
        return;
    }
}
