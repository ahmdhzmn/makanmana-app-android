package com.makanmanaapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;



public class TestFBLoginActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;

    String sfId;
    String sfUsername;
    String sfEmail;
    String sfImageUrl;
    Boolean sfUserLogin = false;

    LoginButton loginButton;
    CircleImageView circleImageView;
    TextView txtFbId, txtFbName, txtFbEmail;

    CallbackManager callbackManager;
    ProfileTracker profileTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_fblogin);

        //  sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);

//        loginButton = findViewById(R.id.fb_login_button);
//        circleImageView = findViewById(R.id.fb_profile_image);
//        txtFbId = findViewById(R.id.fb_id);
//        txtFbName = findViewById(R.id.fb_name);
//        txtFbEmail = findViewById(R.id.fb_email);

        // Check if UserResponse is Already Logged In
//        if (sharedpreferences.contains("userLogin")) {
//            sfUserLogin = sharedpreferences.getBoolean("userLogin", false);
//            if (sfUserLogin) {
//                Intent homescreen = new Intent(TestFBLoginActivity.this, HomeActivity.class);
//                startActivity(homescreen);
//            }
//            else {
//                finish();
//                startActivity(getIntent());
//            }
//
//        }

        callbackManager = CallbackManager.Factory.create();
        loginButton.setPermissions(Arrays.asList("email","public_profile"));

        checkLoginStatus();

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

            }
        };

        profileTracker.startTracking();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Profile profile = Profile.getCurrentProfile();
                checkLoginStatus();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        checkLoginStatus();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        profileTracker.stopTracking();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

//    AccessTokenTracker tokenTracker = new AccessTokenTracker() {
//        @Override
//        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
//            if(currentAccessToken==null){
//                txtFbId.setText("");
//                txtFbEmail.setText("");
//                txtFbName.setText("");
//                circleImageView.setImageResource(0);
//                Toast.makeText(TestFBLoginActivity.this, "User Logged Out", Toast.LENGTH_SHORT).show();
//            }
//            else {
//                loadUserProfile(currentAccessToken);
//            }
//        }
//    };

    private void loadUserProfile(AccessToken newAccessToken) {
        final GraphRequest request = GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String id = object.getString("id");
                    String name = object.getString("name");
                    String email = object.getString("email");
                    String image_url = "https://graph.facebook.com/"+id+"/picture?type=normal";

//                    txtFbId.setText(id);
//                    txtFbName.setText(name);
//                    txtFbEmail.setText(email);
//
//                    sfId = txtFbId.getText().toString();
//                    sfUsername = txtFbName.getText().toString();
//                    sfEmail = txtFbEmail.getText().toString();
//                    sfImageUrl = image_url;

//                    RequestOptions requestOptions = new RequestOptions();
//                    requestOptions.dontAnimate();
//
//                    Glide.with(TestFBLoginActivity.this).load(image_url).into(circleImageView);

                    String displayObject = object.toString();

                    //display object
                    //Toast.makeText(TestFBLoginActivity.this, displayObject, Toast.LENGTH_LONG).show();

//                    SharedPreferences.Editor editor = sharedpreferences.edit();
//                    editor.putString("userId", sfId);
//                    editor.putString("userName", sfUsername);
//                    editor.putString("userEmail", sfEmail);
//                    editor.putString("userImageUrl", sfImageUrl);
//                    editor.putBoolean("userLogin", true);
//                    editor.apply();

//                    Intent homeScreen = new Intent(TestFBLoginActivity.this, HomeActivity.class);
//                    startActivity(homeScreen);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields","id,name,email,likes{category}");
        request.setParameters(parameters);
        request.executeAsync();

    }

    private void checkLoginStatus() {
        if(AccessToken.getCurrentAccessToken()!=null){
            loadUserProfile(AccessToken.getCurrentAccessToken());
        }
    }
}
