package com.makanmanaapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.facebook.FacebookSdk.getApplicationContext;

public class GroupChatFragment extends Fragment {

    SharedPreferences sharedpreferences;

    private EditText editText;

    private View view;
    private RecyclerView chatList;

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    DatabaseReference RootRef = FirebaseDatabase.getInstance().getReference();

    private String userId;

    private ProgressBar spinner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_group_chat, container, false);

        sharedpreferences = getActivity().getSharedPreferences("userPref", Context.MODE_PRIVATE);

        spinner = view.findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        chatList = (RecyclerView) view.findViewById(R.id.group_chat_list);
        chatList.setLayoutManager(new LinearLayoutManager(getContext()));

        Toolbar toolbar = view.findViewById(R.id.toolbar_chat);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        return view;
    }

    private void GenerateRecommendation() {
        String displayText = "We thinks this group should go to . Try it with your friends and you'll not regret!! :)";
    }

    @Override
    public void onStart() {
        super.onStart();

        if (currentUser!= null) {
            userId = currentUser.getUid();
        }

        sharedpreferences = getActivity().getSharedPreferences("userPref", Context.MODE_PRIVATE);

        FirebaseRecyclerOptions<Group> options = new FirebaseRecyclerOptions.Builder<Group>()
                .setQuery(RootRef.child("UserGroups").child(userId), new SnapshotParser<Group>() {
                    @NonNull
                    @Override
                    public Group parseSnapshot(@NonNull DataSnapshot snapshot) {
                        final Group groupDetail = new Group();

                        RootRef.child("UserGroups").child(userId).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                for (DataSnapshot groupDetailSnapshot: dataSnapshot.getChildren()) {

                                    Group group = dataSnapshot.getValue(Group.class);

                                    String groupName = groupDetailSnapshot.getKey().toString();

                                    groupDetail.setGroupName(groupName);
//                                    Log.d("Get Data name", groupName);
//                                    Log.d("Get Data", group.getGroupName());
                                    //  Log.d("mylog", "DataSnapshot (using set): " + dataSnapshot);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        return groupDetail;
                    }
                })
                .build();

        FirebaseRecyclerAdapter<Group, GroupChatViewHolder> adapter =
                new FirebaseRecyclerAdapter<Group, GroupChatViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull final GroupChatViewHolder holder, final int position, @NonNull Group model) {

                        //  TUTORIAL 17
                        //  USEFUL TO ACCESS ALL DATA!!!
//                        RootRef.child("Groups").addValueEventListener(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                                Set<String> set = new HashSet<>();
//                                Iterator iterator = dataSnapshot.getChildren().iterator();
//
//                                while (iterator.hasNext()) {
//                                    set.add(((DataSnapshot) iterator.next()).getKey());
//                                }
//
//
//                                //  Log.d("mylog", "DataSnapshot (using set): " + dataSnapshot);
//                            }
//
//                            @Override
//                            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                            }
//                        });

                        Log.d("mylog", "Model: " + model);
                        //  Log.d("mylog", "Position: " + position);

                        final String retGroupName = getRef(position).getKey();

                        Log.d("mylog", "getRef(position).getKey()" + getRef(position).getKey());

                        holder.txtGroupName.setText(retGroupName);

                        //  holder.txtGroupCreatedBy.setText("boom");

                        RootRef.child("Groups").child(getRef(position).getKey()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                                //  Log.d("mylog", "DataSnapshot here: " + dataSnapshot);

                                //  holder.txtGroupCreatedBy.setText("Group created by: " + dataSnapshot.child("groupCreatedBy").getValue().toString());

                                String retGroupCreatedBy = dataSnapshot.child("groupCreatedBy").getValue().toString();

                                //  Find username of the group creator
                                RootRef.child("Users").child(retGroupCreatedBy).child("profilename").addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                        if(dataSnapshot.exists()) {
                                            holder.txtGroupCreatedBy.setText("Group created by: " + dataSnapshot.getValue().toString());
                                        } else {
                                            holder.txtGroupCreatedBy.setText("Group created by: (Account no longer exist)");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

//                        RootRef.child("UserGroups").child(retGroupName).addValueEventListener(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                                if (dataSnapshot.hasChild(currentUser.getUid())) {
//                                    RootRef.child("GroupUsers").child(retGroupName).addValueEventListener(new ValueEventListener() {
//                                        @Override
//                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                                            //holder.txtGroupName.setText(dataSnapshot.getKey());
//
//                                            String foundGroupName = retGroupName;
//
//                                            RootRef.child("Groups").child(foundGroupName).addValueEventListener(new ValueEventListener() {
//                                                @Override
//                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                                                    String retGroupCreatedBy = dataSnapshot.child("groupCreatedBy").getValue().toString();
//
//                                                    //  Find username of the group creator
//                                                    RootRef.child("Users").child(retGroupCreatedBy).child("profilename").addListenerForSingleValueEvent(new ValueEventListener() {
//                                                        @Override
//                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                                                            holder.txtGroupCreatedBy.setText("Group created by: " + dataSnapshot.getValue().toString());
//                                                        }
//
//                                                        @Override
//                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                                        }
//                                                    });
//
//                                                }
//
//                                                @Override
//                                                public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                                }
//                                            });
//
//
//                                        }
//
//                                        @Override
//                                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                        }
//                                    });
//                                }
//
//
//                                //  holder.txtGroupName.setText(retGroupName);
//
//                                //  Log.d("mylog", "Group group: " + holder.txtGroupName.getText().toString());
//
//                                //  Retrieve groupCreatedBy
//
//                            }
//
//                            @Override
//                            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                            }
//                        });

                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent groupChatScreen = new Intent(getContext(), GroupChatActivity.class);
                                groupChatScreen.putExtra("selectedGroupChat", retGroupName);
                                startActivity(groupChatScreen);
                            }
                        });
                    }

                    @NonNull
                    @Override
                    public GroupChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.groupchat_display_layout, parent, false);
                        return new GroupChatViewHolder(view);
                    }
                };

//        FirebaseRecyclerOptions<Group> options = new FirebaseRecyclerOptions.Builder<Group>()
//                .setQuery(RootRef.child("Groups"), Group.class)
//                .build();
//
//        FirebaseRecyclerAdapter<Group, GroupChatViewHolder> adapter =
//                new FirebaseRecyclerAdapter<Group, GroupChatViewHolder>(options) {
//            @Override
//            protected void onBindViewHolder(@NonNull final GroupChatViewHolder holder, final int position, @NonNull Group model) {
//
//                int checkPosition;
//
//                //  TUTORIAL 17
//                //  USEFUL TO ACCESS ALL DATA!!!
//                RootRef.child("Groups").addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                        Set<String> set = new HashSet<>();
//                        Iterator iterator = dataSnapshot.getChildren().iterator();
//
//                        while (iterator.hasNext()) {
//                            set.add(((DataSnapshot) iterator.next()).getKey());
//                        }
//
//
//                        //  Log.d("mylog", "DataSnapshot (using set): " + dataSnapshot);
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });
//
//                //  Log.d("mylog", "Model: " + model);
//                //  Log.d("mylog", "Position: " + position);
//
//                final String retGroupName = getRef(position).getKey();
//
//                Log.d("mylog", "Group name: " + retGroupName);
//
//                RootRef.child("UserGroups").child(retGroupName).addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                        if (dataSnapshot.hasChild(currentUser.getUid())) {
//                            RootRef.child("GroupUsers").child(retGroupName).addValueEventListener(new ValueEventListener() {
//                                @Override
//                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                                    holder.txtGroupName.setText(dataSnapshot.getKey());
//
//
//                                    String foundGroupName = dataSnapshot.getKey().toString();
//
//                                    RootRef.child("Groups").child(foundGroupName).addValueEventListener(new ValueEventListener() {
//                                        @Override
//                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                                            String retGroupCreatedBy = dataSnapshot.child("groupCreatedBy").getValue().toString();
//
//                                            //  Find username of the group creator
//                                            RootRef.child("Users").child(retGroupCreatedBy).child("profilename").addListenerForSingleValueEvent(new ValueEventListener() {
//                                                @Override
//                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                                                    holder.txtGroupCreatedBy.setText("Group created by: " + dataSnapshot.getValue().toString());
//                                                }
//
//                                                @Override
//                                                public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                                }
//                                            });
//
//                                        }
//
//                                        @Override
//                                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                        }
//                                    });
//
//
//                                }
//
//                                @Override
//                                public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                }
//                            });
//                        }
//
//
//                        //  holder.txtGroupName.setText(retGroupName);
//
//                        //  Log.d("mylog", "Group group: " + holder.txtGroupName.getText().toString());
//
//                        //  Retrieve groupCreatedBy
//
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });
//
//
//
//                holder.itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        Intent groupChatScreen = new Intent(getContext(), GroupChatActivity.class);
//                        groupChatScreen.putExtra("selectedGroupChat", retGroupName);
//                        startActivity(groupChatScreen);
//                    }
//                });
//            }
//
//            @NonNull
//            @Override
//            public GroupChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.groupchat_display_layout, parent, false);
//                return new GroupChatViewHolder(view);
//            }
//        };

        chatList.setAdapter(adapter);
        adapter.startListening();
    }

    public static class GroupChatViewHolder extends RecyclerView.ViewHolder {

        CircleImageView groupProfileImage;
        TextView txtGroupName, txtGroupCreatedBy;

        public GroupChatViewHolder(@NonNull View itemView) {
            super(itemView);

            txtGroupName = itemView.findViewById(R.id.group_name_display);
            txtGroupCreatedBy = itemView.findViewById(R.id.group_createdby_display);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.option_toolbar_chat, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch(item.getItemId()) {
            case R.id.create_group_option:
                //  Toast.makeText(getActivity(),"Create group..", Toast.LENGTH_LONG).show();

                RequestNewGroup();
                break;
            case R.id.find_groups_option:
                Intent findGroupsScreen = new Intent(getContext(), FindGroupsActivity.class);
                startActivity(findGroupsScreen);
                break;
            case R.id.view_users_option:
                Intent viewUsersScreen = new Intent(getContext(), FindFriendsActivity.class);
                startActivity(viewUsersScreen);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void RequestNewGroup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Enter Group Name:");

        final EditText groupNameField = new EditText(getActivity());
        groupNameField.setHint("e.g Geng Merlimau");
        builder.setView(groupNameField);

        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String groupName = groupNameField.getText().toString().trim();
                if (TextUtils.isEmpty(groupName)) {
                    Toast.makeText(getContext(), "Please enter a group name!", Toast.LENGTH_LONG).show();
                }
                else {
                    spinner.setVisibility(View.VISIBLE);
                    CreateNewGroup(groupName);
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.show();
    }

    private void CreateNewGroup(final String groupName) {

        final HashMap<String,String> profileMap = new HashMap<>();
        profileMap.put("groupCreatedBy", FirebaseAuth.getInstance().getCurrentUser().getUid());

        //  Check if group name already exist
        RootRef.child("Groups").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(groupName)) {
                    spinner.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Ooops! The group name is already exist in our system. Please try other name.", Toast.LENGTH_LONG).show();
                }
                else {
                    //  Insert group data into firebase
                    RootRef.child("Groups").child(groupName).setValue(profileMap)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()) {
                                        RootRef.child("Groups").child(groupName).child("Users")
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                .setValue("").addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()) {

                                                    RootRef.child("UserGroups").child(userId).child(groupName).setValue("").addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(task.isSuccessful()) {
                                                                spinner.setVisibility(View.GONE);
                                                                Toast.makeText(getContext(), groupName + " is Created", Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    });
                                                }
                                                else
                                                    spinner.setVisibility(View.GONE);
                                                    Toast.makeText(getContext(), "Error Creating Group! Pls try again later", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                    else {
                                        spinner.setVisibility(View.GONE);
                                        Toast.makeText(getContext(), "Error Creating Group! Pls try again later", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
