package com.makanmanaapp;

public class UserDisplay {

    private String profilename, username;

    public UserDisplay() {
    }

    public UserDisplay(String profilename, String username) {
        this.profilename = profilename;
        this.username = username;
    }

    public String getProfilename() {
        return profilename;
    }

    public void setProfilename(String profilename) {
        this.profilename = profilename;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
