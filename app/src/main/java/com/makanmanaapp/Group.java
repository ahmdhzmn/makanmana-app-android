package com.makanmanaapp;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Group {


    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    String currentUserId = currentUser.getUid().toString();

    private String groupName, groupDateCreated, groupCreatedBy;
    private Boolean groupValue;

    public Group() {

    }

//    public Group(String groupName, String groupDateCreated, String groupCreatedBy) {
//        this.groupName = groupName;
//        this.groupDateCreated = groupDateCreated;
//        this.groupCreatedBy = groupCreatedBy;
//    }

    public Group(String groupName, String groupCreatedBy) {
        this.groupName = groupName;
        this.groupCreatedBy = groupCreatedBy;
        //  this.currentUserId = currentUserId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDateCreated() {
        return groupDateCreated;
    }

    public void setGroupDateCreated(String groupDateCreated) {
        this.groupDateCreated = groupDateCreated;
    }

    public String getGroupCreatedBy() {
        return groupCreatedBy;
    }

    public void setGroupCreatedBy(String groupCreatedBy) {
        this.groupCreatedBy = groupCreatedBy;
    }

    public String getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(String currentUserId) {
        this.currentUserId = currentUserId;
    }

//    public String findGroupCreatedBy(String userId) {
//
//    }
}
