package com.makanmanaapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RecommendationResultActivity extends AppCompatActivity {

    // Foursquare CLIENT_ID and CLIENT_SECRET
    final String CLIENT_ID = "N5ZKCYCWUKNJUWQ04D5Z3QTTSRFKC1BGA2EMU4CK1GTRW1QE";
    final String CLIENT_SECRET = "41WSZPKHNAOK3YJEDYVCVB4ETQTE1MPXHVGOAIVJUWNK0S4G";

    //  private RequestQueue requestQueue;

    private String MainPreference, SecondPreference, ThirdPreference, FourthPreference;
    private String ScoreResult;
    private JSONArray ScoreResultArray;
    private RecyclerView recyclerView;

    private int counter = 0, counterRestaurant = 0;

    String RestaurantImageUrl, RestaurantName, RestaurantCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommendation_result);

        ScoreResult = getIntent().getExtras().get("ScoreResult").toString();
        try {
            ScoreResultArray = new JSONArray(ScoreResult);
            Log.d("Group Preference Score in Recommend Activity", ": " + ScoreResultArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        recyclerView = findViewById(R.id.recommendation_result_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //  Loop Result Array
//        for (int i = 0; i<ScoreResultArray.length(); i++) {
//            try {
//                JSONObject PreferenceObject = ScoreResultArray.getJSONObject(i);
//                String PreferenceCategory = PreferenceObject.names().getString(0);
//
//                if (i==0) {
//                    String PreferenceType = "Main Preference";
//                    CallFoursquareAPI(PreferenceCategory, PreferenceType);
//
//                } else if (i==1) {
//                    String PreferenceType = "Second Preference";
//                    CallFoursquareAPI(PreferenceCategory, PreferenceType);
//
//                }else if (i==2) {
//                    String PreferenceType = "Third Preference";
//                    CallFoursquareAPI(PreferenceCategory, PreferenceType);
//
//                }else if (i==3) {
//                    String PreferenceType = "Fourth Preference";
//                    CallFoursquareAPI(PreferenceCategory, PreferenceType);
//
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }

        ArrayList<Restaurant> RestaurantArrayList = new ArrayList<>();
        CallFoursquareAPI("Western Restaurant", RestaurantArrayList);
    }

    @Override
    protected void onStart() {
        super.onStart();

        ArrayList<Restaurant> RestaurantArrayList = new ArrayList<>();
        CallFoursquareAPI("Western Restaurant", RestaurantArrayList);
    }

    @Override
    protected void onResume() {
        super.onResume();

        ArrayList<Restaurant> RestaurantArrayList = new ArrayList<>();
        CallFoursquareAPI("Western Restaurant", RestaurantArrayList);
    }

    private void CallFoursquareAPI(String PreferenceCategory, final ArrayList<Restaurant> restaurantArrayList) {

        String url="https://api.foursquare.com/v2/venues/search?client_id="
                +CLIENT_ID
                +"&client_secret="
                +CLIENT_SECRET
                +"&v=20191128&near=Shah Alam,Selangor&query="+PreferenceCategory+"&limit=10";

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("Restaurant Response", "" + response);
                try {
                    JSONObject VenuesResponse = response.getJSONObject("response");
                    JSONArray VenuesArray = VenuesResponse.getJSONArray("venues");

                    counterRestaurant = VenuesArray.length();
                    final String[] RestaurantNameArray = new String[counterRestaurant];
                    final String[] RestaurantCategoryArray = new String[counterRestaurant];

                    for (int i=0; i<VenuesArray.length(); i++) {
                        JSONObject Venue = VenuesArray.getJSONObject(i);

                        //  Get Venue Id
                        String RestaurantId = Venue.getString("id");
                        //Log.d("Restaurant Id", "" + RestaurantId);

                        //String RestaurantImageUrl = "https://igx.4sqi.net/img/general/500x500/5163668_xXFcZo7sU8aa1ZMhiQ2kIP7NllD48m7qsSwr1mJnFj4.jpg";
                        //String RestaurantImageUrl = "https://igx.4sqi.net/img/general/500x500/5163668_xXFcZo7sU8aa1ZMhiQ2kIP7NllD48m7qsSwr1mJnFj4.jpg";

                        //  Get Restaurant Name
                        RestaurantName = Venue.getString("name");
                        RestaurantNameArray[i] = RestaurantName;
                        //  Log.d("Restaurant Name", "" + RestaurantName);
                        
                        //  Get Venue Category
                        JSONArray Categories = Venue.getJSONArray("categories");
                        JSONObject Category = Categories.getJSONObject(0);
                        RestaurantCategory = Category.getString("name");
                        RestaurantCategoryArray[i] = RestaurantCategory;
                        //  Log.d("Restaurant Category", "" + RestaurantCategory);

//                        if (RestaurantCategory == PreferedRestaurant) {
//                            //  Amik RestaurantId, RestaurantName, RestaurantAddress
//                        }

                        String PlaceSearchUrl = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?key="+getString(R.string.places_api_key)+"&input="+RestaurantName+"&inputtype=textquery&fields=photos";

                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        JsonObjectRequest request = new JsonObjectRequest(
                                Request.Method.GET, PlaceSearchUrl, null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                Log.d("Place search with photo response","" + response);

                                try {
                                    JSONArray Candidates = response.getJSONArray("candidates");
                                    if (Candidates.length() == 0 || Candidates == null) {
                                        RestaurantImageUrl = "http://cdn.onlinewebfonts.com/svg/img_571859.png";
                                    } else {
                                        JSONObject CandidateObject = Candidates.getJSONObject(0);
                                        JSONArray Photos = CandidateObject.getJSONArray("photos");
                                        JSONObject PhotoObject = Photos.getJSONObject(0);
                                        String PhotoReference = PhotoObject.getString("photo_reference");

                                        Log.d("Photo Reference","" + PhotoReference);

                                        RestaurantImageUrl = "https://maps.googleapis.com/maps/api/place/photo?maxheight=100&photoreference="+PhotoReference+"&key="+getString(R.string.places_api_key);
                                    }

                                    restaurantArrayList.add(new Restaurant(RestaurantImageUrl, RestaurantNameArray[counter], RestaurantCategoryArray[counter]));
                                    counter++;

                                    if (counter == counterRestaurant) {
                                        Log.d("Counter","" + counter);
                                        counter = 0;
                                        RestaurantAdapter restaurantAdapter = new RestaurantAdapter(RecommendationResultActivity.this, restaurantArrayList);
                                        recyclerView.setAdapter(restaurantAdapter);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                            }
                        });

                        requestQueue.add(request);

                        //  restaurantArrayList.add(new Restaurant(RestaurantId, RestaurantName, RestaurantCategory));
                    }

//                    restaurantAdapter = new RestaurantAdapter(RecommendationResultActivity.this, restaurantArrayList);
//                    recyclerView.setAdapter(restaurantAdapter);

                    //GetRestaurantImageAndDisplayResult(restaurantArrayList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(request);
    }

    private void GetRestaurantImageAndDisplayResult(final ArrayList<Restaurant> restaurantArrayList) {

         do {
            final String RestaurantId = restaurantArrayList.get(counter).getImageUrl();
            String url = "https://api.foursquare.com/v2/venues/"+RestaurantId+"/photos?client_id="+CLIENT_ID+"&client_secret="+CLIENT_SECRET+"&v=20191128";

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        counter++;

                        JSONObject ImageResponse = response.getJSONObject("response");
                        JSONObject PhotosObject = ImageResponse.getJSONObject("photos");
                        JSONArray ItemsArray = ImageResponse.getJSONArray("items");

                        JSONObject ItemObject = ItemsArray.getJSONObject(0);

                        String ImageUrl = ItemObject.getString("prefix");
                        ImageUrl = ImageUrl + "500x500" + ItemObject.getString("suffix");

                        restaurantArrayList.get(counter).setImageUrl(ImageUrl);

                        if (counter == (restaurantArrayList.size()-1)) {
                            RestaurantAdapter restaurantAdapter = new RestaurantAdapter(RecommendationResultActivity.this, restaurantArrayList);
                            recyclerView.setAdapter(restaurantAdapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });

            requestQueue.add(request);
        }
        while (counter != (restaurantArrayList.size()-1));
    }
}
