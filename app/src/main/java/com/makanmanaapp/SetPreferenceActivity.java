package com.makanmanaapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SetPreferenceActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    SharedPreferences sharedpreferences;

    Toolbar toolbar;

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    DatabaseReference RootRef = FirebaseDatabase.getInstance().getReference();

    Spinner spinnerMainPref, spinnerPref2, spinnerPref3, spinnerPref4;

    int positionMainPref, positionSecondPref, positionThirdPref, positionFourthPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_preference);

        List<CharSequence> Preferences = new ArrayList<CharSequence>(Arrays.asList(getResources().getStringArray(R.array.preferences)));

        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);

        toolbar = findViewById(R.id.toolbar_set_preference);
        setSupportActionBar(toolbar);

        //  Card Background image
        RoundCornerDrawable round = new RoundCornerDrawable(BitmapFactory.decodeResource(getResources(),R.drawable.background_card),
                getResources().getDimension(R.dimen.cardview_radius), 0);
        ImageView imageView = findViewById(R.id.backgroundimageSetPreference);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            imageView.setBackground(round);
        else
            imageView.setBackgroundDrawable(round);

        RoundCornerDrawable round2 = new RoundCornerDrawable(BitmapFactory.decodeResource(getResources(),R.drawable.background_card),
                getResources().getDimension(R.dimen.cardview_radius), 0);
        ImageView imageView2= findViewById(R.id.backgroundimageSetPreference2);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            imageView2.setBackground(round2);
        else
            imageView2.setBackgroundDrawable(round2);
        //  Card Background image


//        final ArrayAdapter<CharSequence> adapterPreferences = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, Preferences) {
//            @Override
//            public boolean isEnabled(int position) {
//
//                if (spinnerMainPref.getSelectedItemPosition() == position || spinnerPref2.getSelectedItemPosition() == position
//                    || spinnerPref3.getSelectedItemPosition() == position) {
//                    return false;
//                }
//                if (spinnerMainPref.getSelectedItemPosition() == position || spinnerPref3.getSelectedItemPosition() == position
//                        || spinnerPref4.getSelectedItemPosition() == position) {
//                    return false;
//                }
//                if (spinnerMainPref.getSelectedItemPosition() == position || spinnerPref4.getSelectedItemPosition() == position
//                        || spinnerPref4.getSelectedItemPosition() == position) {
//                    return false;
//                }
//                if (spinnerPref2.getSelectedItemPosition() == position || spinnerPref3.getSelectedItemPosition() == position
//                        || spinnerPref4.getSelectedItemPosition() == position) {
//                    return false;
//                }
//                return true;
//            }
//
//            @Override
//            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//
//                switch(parent.getId()) {
//                    case R.id.main_preference_spinner:
//
//                        spinnerPref2.setSelected();
//                        String valueMainPref = adapterView.getItemAtPosition(i).toString();
//                        //  textView1.setText(valueMainPref);
//
//                        editor.putString("mainPreference", valueMainPref);
//                        editor.putInt("mainPreferenceValuePosition", i);
//                        editor.apply();
//                        c
//
//
//                        Log.d("Main Preference:", " " + sharedpreferences.getString("mainPreference", null));
//
//                        break;
//
//                }
//
//                View mView = super.getDropDownView(position, convertView, parent);
//                TextView mTextView = (TextView) mView;
//                if (year <= max_year && position < max_month - 1) {
//                    mTextView.setTextColor(Color.GRAY);
//                } else {
//                    mTextView.setTextColor(Color.BLACK);
//                }
//                return mView;
//
//                //  return super.getDropDownView(position, convertView, parent);
//            }
//        };
//        adapterPreferences.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //  Adapater Original
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.preferences, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerMainPref = findViewById(R.id.main_preference_spinner);
        spinnerMainPref.setAdapter(adapter);
//        spinnerMainPref.setSelection(0, false);
//        spinnerMainPref.setOnItemSelectedListener(this);

        //  Kalau get data preference from firebase use this
        //  spinnerMainPref.setSelection(getIndex(spinnerMainPref, "Bar & Grill"));

        spinnerPref2 = findViewById(R.id.preference2_spinner);
        spinnerPref2.setAdapter(adapter);
//        spinnerPref2.setSelection(0, false);
//        spinnerPref2.setOnItemSelectedListener(this);

        spinnerPref3 = findViewById(R.id.preference3_spinner);
        spinnerPref3.setAdapter(adapter);
//        spinnerPref3.setSelection(0, false);
//        spinnerPref3.setOnItemSelectedListener(this);

        spinnerPref4 = findViewById(R.id.preference4_spinner);
        spinnerPref4.setAdapter(adapter);
//        spinnerPref4.setSelection(0, false);
//        spinnerPref4.setOnItemSelectedListener(this);

        //  RootRef.child("Users").("Preferences")

    }

    @Override
    protected void onResume() {
        super.onResume();

        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);

        if (sharedpreferences.contains("mainPreference")) {
            if (sharedpreferences.getString("mainPreference", "").equals("")) {
                spinnerMainPref.setSelection(0, false);
                spinnerMainPref.setOnItemSelectedListener(this);
            } else {
                spinnerMainPref.setSelection(getIndex(spinnerMainPref, sharedpreferences.getString("mainPreference", "")), false);
                spinnerMainPref.setOnItemSelectedListener(this);
            }
        } else {
            spinnerMainPref.setSelection(0, false);
            spinnerMainPref.setOnItemSelectedListener(this);
        }

        if (sharedpreferences.contains("secondPreference")) {
            if (sharedpreferences.getString("secondPreference", "").equals("")) {
                spinnerPref2.setSelection(0, false);
                spinnerPref2.setOnItemSelectedListener(this);
            } else {
                spinnerPref2.setSelection(getIndex(spinnerPref2, sharedpreferences.getString("secondPreference", "")), false);
                spinnerPref2.setOnItemSelectedListener(this);
            }
        } else {
            spinnerPref2.setSelection(0, false);
            spinnerPref2.setOnItemSelectedListener(this);
        }

        if (sharedpreferences.contains("thirdPreference")) {
            if (sharedpreferences.getString("thirdPreference", "").equals("")) {
                spinnerPref3.setSelection(0, false);
                spinnerPref3.setOnItemSelectedListener(this);
            } else {
                spinnerPref3.setSelection(getIndex(spinnerPref3, sharedpreferences.getString("thirdPreference", "")), false);
                spinnerPref3.setOnItemSelectedListener(this);
            }
        } else {
            spinnerPref3.setSelection(0, false);
            spinnerPref3.setOnItemSelectedListener(this);
        }

        if (sharedpreferences.contains("fourthPreference")) {
            if (sharedpreferences.getString("fourthPreference", "").equals("")) {
                spinnerPref4.setSelection(0, false);
                spinnerPref4.setOnItemSelectedListener(this);
            } else {
                spinnerPref4.setSelection(getIndex(spinnerPref4, sharedpreferences.getString("fourthPreference", "")), false);
                spinnerPref4.setOnItemSelectedListener(this);
            }
        } else {
            spinnerPref4.setSelection(0, false);
            spinnerPref4.setOnItemSelectedListener(this);
        }

//        if (sharedpreferences.contains("mainPreferenceValuePosition")) {
//            spinnerMainPref.setSelection(sharedpreferences.getInt("mainPreferenceValuePosition", 0), false);
//            spinnerMainPref.setOnItemSelectedListener(this);
//        } else {
//            spinnerMainPref.setSelection(0, false);
//            spinnerMainPref.setOnItemSelectedListener(this);
//        }
//
//        if (sharedpreferences.contains("secondPreferenceValuePosition")) {
//            spinnerPref2.setSelection(sharedpreferences.getInt("secondPreferenceValuePosition", 0), false);
//            spinnerPref2.setOnItemSelectedListener(this);
//        } else {
//            spinnerPref2.setSelection(0, false);
//            spinnerPref2.setOnItemSelectedListener(this);
//        }
//
//        if (sharedpreferences.contains("thirdPreferenceValuePosition")) {
//            spinnerPref3.setSelection(sharedpreferences.getInt("thirdPreferenceValuePosition", 0), false);
//            spinnerPref3.setOnItemSelectedListener(this);
//        } else {
//            spinnerPref3.setSelection(0, false);
//            spinnerPref3.setOnItemSelectedListener(this);
//        }
//
//        if (sharedpreferences.contains("fourthPreferenceValuePosition")) {
//            spinnerPref4.setSelection(sharedpreferences.getInt("fourthPreferenceValuePosition", 0), false);
//            spinnerPref4.setOnItemSelectedListener(this);
//        } else {
//            spinnerPref4.setSelection(0, false);
//            spinnerPref4.setOnItemSelectedListener(this);
//        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        switch (adapterView.getId()) {

            case R.id.main_preference_spinner:
                positionMainPref = adapterView.getSelectedItemPosition();
                String valueMainPref = adapterView.getItemAtPosition(i).toString();
                //  textView1.setText(valueMainPref);

                editor.putString("mainPreference", valueMainPref);
                editor.putInt("mainPreferenceValuePosition", i);
                editor.apply();

                RootRef.child("Users").child(currentUser.getUid()).child("Preferences")
                        .child("mainpreference").setValue(valueMainPref).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(SetPreferenceActivity.this,"Main Preference Saved", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                Log.d("Main Preference:", " " + sharedpreferences.getString("mainPreference", null));
                break;

            case R.id.preference2_spinner:
                String valuePref2 = adapterView.getItemAtPosition(i).toString();
                //  textView2.setText(valuePref2);

                editor.putString("secondPreference", valuePref2);
                editor.putInt("secondPreferenceValuePosition", i);
                editor.apply();

                RootRef.child("Users").child(currentUser.getUid()).child("Preferences")
                        .child("secondpreference").setValue(valuePref2).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(SetPreferenceActivity.this,"Second Preference Saved", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                Log.d("Second Preference:", " " + sharedpreferences.getString("secondPreference", null));
                break;

            case R.id.preference3_spinner:
                String valuePref3 = adapterView.getItemAtPosition(i).toString();
                //  textView3.setText(valuePref3);

                editor.putString("thirdPreference", valuePref3);
                editor.putInt("thirdPreferenceValuePosition", i);
                editor.apply();

                RootRef.child("Users").child(currentUser.getUid()).child("Preferences")
                        .child("thirdpreference").setValue(valuePref3).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(SetPreferenceActivity.this,"Third Preference Saved", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                Log.d("Third Preference:", " " + sharedpreferences.getString("thirdPreference", null));
                break;

            case R.id.preference4_spinner:
                String valuePref4 = adapterView.getItemAtPosition(i).toString();
                //  textView4.setText(valuePref4);

                editor.putString("fourthPreference", valuePref4);
                editor.putInt("fourthPreferenceValuePosition", i);
                editor.apply();

                RootRef.child("Users").child(currentUser.getUid()).child("Preferences")
                        .child("fourthpreference").setValue(valuePref4).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(SetPreferenceActivity.this,"Fourth Preference Saved", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                Log.d("Fourth Preference:", " " + sharedpreferences.getString("fourthPreference", null));

                break;
        }
//        String text = adapterView.getItemAtPosition(i).toString();
//        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//
////        if (sharedpreferences.contains("mainPreference")) {
////            spinnerMainPref.setSelection(sharedpreferences.getInt("mainPreferenceValuePosition", 0));
////        }
//        if (sharedpreferences.contains("secondPreference")) {
//            spinnerPref2.setSelection(sharedpreferences.getInt("secondPreferenceValuePosition", 0));
//        }
//        if (sharedpreferences.contains("thirdPreference")) {
//            spinnerPref3.setSelection(sharedpreferences.getInt("thirdPreferenceValuePosition", 0));
//        }
//        if (sharedpreferences.contains("fourthPreference")) {
//            spinnerPref4.setSelection(sharedpreferences.getInt("fourthPreferenceValuePosition", 0));
//        }
//    }

    private int getIndex(Spinner spinner, String restaurantType){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(restaurantType)){
                index = i;
            }
        }
        return index;
    }

    @Override
    public void onBackPressed() {

        //  super.onBackPressed(); - Call function go to previous activity android

        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);

        if (sharedpreferences.getInt("mainPreferenceValuePosition", 0) == 0 ||
                sharedpreferences.getInt("secondPreferenceValuePosition", 0) == 0 ||
                sharedpreferences.getInt("thirdPreferenceValuePosition", 0) == 0 ||
                sharedpreferences.getInt("fourthPreferenceValuePosition", 0) == 0) {
            Toast.makeText(SetPreferenceActivity.this,"Please set all preferences", Toast.LENGTH_SHORT).show();
        }
        else {
            startActivity(new Intent(SetPreferenceActivity.this, MainActivity.class));
            finish();
        }
    }
}
