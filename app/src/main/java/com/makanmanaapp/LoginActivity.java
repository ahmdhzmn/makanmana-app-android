package com.makanmanaapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.transform.Result;

public class LoginActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;

    RequestQueue requestQueue;

    private FirebaseAuth mAuth;

    FusedLocationProviderClient fusedLocationProviderClient;
    private static final int REQUEST_CODE = 101;
    Location currentLocation;
    LocationManager locationManager;
    String myLocation, myCoordinates;


//    String sfId;
//    String sfUsername;
//    String sfEmail;
//    String sfImageUrl;
//    Boolean sfUserLogin = false;

    //  CallbackManager callbackManager;
    ProfileTracker profileTracker;

    private LinearLayout mainLayout;

    TextView txtRegister;
    EditText txtEmailLogin, txtPasswordLogin;
    Button loginButton;

    LoginButton fbloginButton;
    CallbackManager mCallbackManager;

    ProgressBar spinner;

    DatabaseReference RootRef = FirebaseDatabase.getInstance().getReference();

    HashMap<String, Object> preferenceMap = new HashMap<>();
    String mainpreference, secondpreference, thirdpreference, fourthpreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);

        //  To get User's Location
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fetchLastLocation();

        //  Initialize fields
        mainLayout = findViewById(R.id.login_main_layout);

        txtEmailLogin = findViewById(R.id.txt_login_email);;
        txtPasswordLogin = findViewById(R.id.txt_login_password);;
        loginButton = findViewById(R.id.login_button);
        txtRegister = findViewById(R.id.txt_register_now);;
        spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        //  Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();
        fbloginButton = findViewById(R.id.fb_login_button);
        fbloginButton.setPermissions("email", "public_profile");
        fbloginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("Facebook Login", "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d("Facebook Login", "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("Facebook Login", "facebook:onError", error);
                // ...
            }
        });

        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                Intent registerScreen = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(registerScreen);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);

                spinner.setVisibility(View.VISIBLE);

                final String email = txtEmailLogin.getText().toString().trim();
                final String password = txtPasswordLogin.getText().toString().trim();

                if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
                    spinner.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this,"Please fill in login details!", Toast.LENGTH_LONG).show();
                }
                else {
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {

                                        //  Save user data into SharedPreferences
                                        RootRef.child("Users").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                                editor.putString("userId", mAuth.getCurrentUser().getUid());
                                                editor.putString("userName", dataSnapshot.child("username").getValue().toString());
                                                editor.putString("profileName", dataSnapshot.child("profilename").getValue().toString());
                                                editor.putString("userEmail", dataSnapshot.child("email").getValue().toString());
                                                editor.putString("userNumberOfRecommend", dataSnapshot.child("requestedrecommendation").getValue().toString());
                                                editor.putString("userType", "1");
                                                editor.apply();

                                                Intent mainScreen = new Intent(LoginActivity.this, MainActivity.class);
                                                mainScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                Toast.makeText(LoginActivity.this,"Logged In Successful", Toast.LENGTH_SHORT).show();
                                                spinner.setVisibility(View.GONE);
                                                startActivity(mainScreen);
                                                finish();
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });


                                    }
                                    else {
                                        String message = task.getException().toString();
                                        spinner.setVisibility(View.GONE);
                                        Toast.makeText(LoginActivity.this, "Incorrect Email or Password. Please Try Again.", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }

            }
        });

//        callbackManager = CallbackManager.Factory.create();
//
//        fbloginButton.setPermissions(Arrays.asList("email","public_profile"));
//
//        checkLoginStatus();
//
//        profileTracker = new ProfileTracker() {
//            @Override
//            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
//
//            }
//        };
//
//        profileTracker.startTracking();

//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Profile profile = Profile.getCurrentProfile();
//                checkLoginStatus();
//            }
//
//            @Override
//            public void onCancel() {
//
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//
//            }
//        });
    }

    private void fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_CODE);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location != null) {
                    currentLocation = location;
                    //  Toast.makeText(getApplicationContext(),currentLocation.getLatitude() + " " + currentLocation.getLongitude(), Toast.LENGTH_LONG).show();
                }

                LatLng myCoordinates = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());

                // Call method to display user location
                getCityName(myCoordinates);

                //  Call method to display user coordinates
                String userCoordinates = myCoordinates.toString();
                //  passUserCoordinates(userCoordinates);

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE:
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLastLocation();
                }
                break;
        }
    }

    private void getCityName(LatLng userCoordinates) {
        String myCity = "";
        String myState = "";
        String myLatitude = "";
        String myLongitude = "";
        myLocation = "";
        myCoordinates = "";
        Geocoder geocoder = new Geocoder(LoginActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(userCoordinates.latitude, userCoordinates.longitude, 1);
            String address = addresses.get(0).getAddressLine(0);
            myCity = addresses.get(0).getLocality();
            myState = addresses.get(0).getAdminArea();
            myLatitude = String.format ("%.7f", addresses.get(0).getLatitude());
            myLongitude = String.format ("%.7f", addresses.get(0).getLongitude());
            //myFullAddress = addresses.get(0).getAddressLine(0);
            Log.d("mylog", "Complete Address: " + addresses.toString());
            Log.d("mylog", "Address: " + address);
        } catch (IOException e) {
            e.printStackTrace();
        }
        myLocation = myCity + ", " + myState;
        myCoordinates = "(" + myLatitude + ", " + myLongitude + ")";
        // return myLocation;

        // passUserLocation(myLocation);

        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("userLocation", myLocation);
        editor.putString("userCoordinates", myCoordinates);
        editor.apply();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFacebookAccessToken(AccessToken token) {
        spinner.setVisibility(View.VISIBLE);
        graphRequestProfile(token);
        Log.d("Facebook Login", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Facebook Login", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            //  Insert count vectorization data
                            RootRef.child("Users").child(user.getUid()).child("Preferences").setValue(preferenceMap);

                            //  Check for userId is exist in realtime database
                            RootRef.child("Users").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                    if (dataSnapshot.exists()) {

                                        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedpreferences.edit();
                                        editor.putString("userId", mAuth.getCurrentUser().getUid());
                                        editor.putString("userName", dataSnapshot.child("username").getValue().toString());
                                        editor.putString("profileName", dataSnapshot.child("profilename").getValue().toString());
                                        editor.putString("userEmail", dataSnapshot.child("email").getValue().toString());
                                        editor.putString("userNumberOfRecommend", dataSnapshot.child("requestedrecommendation").getValue().toString());
                                        editor.putString("userType", "2");
                                        editor.putBoolean("isLogin", true);
                                        editor.apply();

                                        Intent mainScreen = new Intent(LoginActivity.this, MainActivity.class);
                                        mainScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        spinner.setVisibility(View.GONE);
                                        startActivity(mainScreen);
                                        Toast.makeText(LoginActivity.this,"Logged In Successful", Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                    else {
//                                        FirebaseAuth.getInstance().signOut();
//                                        LoginManager.getInstance().logOut();
//                                        finish();
                                        Intent firstTimeLoginScreen = new Intent(LoginActivity.this, NewFacebookUserActivity.class);
                                        firstTimeLoginScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        spinner.setVisibility(View.GONE);
                                        startActivity(firstTimeLoginScreen);
                                        finish();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                            //  updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Facebook Login", "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //  updateUI(null);
                        }

                        // ...
                    }
                });
    }

//    @Override
//    protected void onResume()
//    {
//        super.onResume();
//        Profile profile = Profile.getCurrentProfile();
//        checkLoginStatus();
//    }

//    @Override
//    protected void onPause()
//    {
//        super.onPause();
//    }
//
//    @Override
//    protected void onStop()
//    {
//        super.onStop();
//        profileTracker.stopTracking();
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode == MY_REQUEST_CODE) {
//            IdpResponse response = IdpResponse.fromResultIntent(data);
//            if(resultCode == RESULT_OK) {
//                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//                Toast.makeText(this, "FB User: " + user.getEmail(), Toast.LENGTH_SHORT).show();
//            }
//            else {
//                Toast.makeText(this, "Error: " + response.getError().getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        }
//    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        callbackManager.onActivityResult(requestCode,resultCode,data);
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    private void graphRequestProfile(final AccessToken token)
    {
        final GraphRequest request= GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                Log.d("Fb data: ", "Graph API Data: " + object);
                //  Toast.makeText(LoginActivity.this, object.toString(), Toast.LENGTH_LONG).show();

                try {
                    fbloginButton.setVisibility(View.GONE);

                    String id = object.getString("id");
                    String email = object.getString("email");
                    String name = object.getString("name");
                    //  String image_url = "https://graph.facebook.com/"+id+"/picture?type=normal";

                    sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("userId", id);
                    editor.putString("userEmail", email);
                    editor.putString("profileName", name);
                    //  editor.putString("userImageUrl", image_url);
                    editor.putString("userType", "2");
                    editor.putString("userToken", token.toString());
                    editor.apply();

                    String fbJsonData = object.toString();
                    //Toast.makeText(LoginActivity.this, fbJsonData, Toast.LENGTH_LONG).show();

                    //  Post User's FB Data To This Method
                    PostFbUserData(object);
                    //  Toast.makeText(LoginActivity.this, fbJsonData, Toast.LENGTH_LONG).show();

//                    Intent MainScreen = new Intent(LoginActivity.this, MainActivity.class);
//                    MainScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(MainScreen);
//                    finish();
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields","id,name,email,likes.limit(100){category}");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void PostFbUserData(JSONObject data) {

        //  String url="http://172.20.10.4:8000/api/store_fb_user";

        String url="http://makanmana.ml/api/store_fb_user";
        //  String url="http://192.168.0.182:8000/api/store_fb_user";

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, url, data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Laravel Login Response", response.toString());
                GetFbUserCountVectorizationData();

                //  Toast.makeText(getApplicationContext(),response.toString(),Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  VolleyLog.d("Error", "Error: " + error.getMessage());
                Log.d("Laravel Login Error Response", error.getMessage());
                //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    public void GetFbUserCountVectorizationData() {

        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
        String fb_id = sharedpreferences.getString("userId", null);

        String url="http://makanmana.ml/api/count_vectorization/"+fb_id;

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("Response Data", "Count Vectorization Data: " + response.toString());

                try {
                    mainpreference = response.getString("mainpreference");
                    secondpreference = response.getString("secondpreference");
                    thirdpreference = response.getString("thirdpreference");
                    fourthpreference = response.getString("fourthpreference");

                    preferenceMap.put("mainpreference", mainpreference);
                    preferenceMap.put("secondpreference", secondpreference);
                    preferenceMap.put("thirdpreference", thirdpreference);
                    preferenceMap.put("fourthpreference", fourthpreference);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Count Vectorization Error", error.getMessage());
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
