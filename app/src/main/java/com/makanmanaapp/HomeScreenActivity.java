package com.makanmanaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeScreenActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;

    String id, email, name, imageUrl;

    CircleImageView circleImageView;

    TextView welcomeText, txtFbId, txtFbName, txtFbEmail;
    Button buttonLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        FacebookSdk.sdkInitialize(this);

        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);

        id = sharedpreferences.getString("userId", "");
        email = sharedpreferences.getString("userEmail", "");
        name = sharedpreferences.getString("userName", "");
        imageUrl = sharedpreferences.getString("userImageUrl", "");

        welcomeText = (TextView) findViewById(R.id.welcomeText);
        circleImageView = findViewById(R.id.fb_profile_image);
        txtFbId = (TextView) findViewById(R.id.fb_id);
        txtFbEmail = (TextView) findViewById(R.id.fb_email);
        txtFbName = (TextView) findViewById(R.id.fb_name);

        welcomeText.setText("Welcome, " + name + "!");
        txtFbId.setText(id);
        txtFbEmail.setText(email);
        txtFbName.setText(name);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.dontAnimate();

        Glide.with(HomeScreenActivity.this).load(imageUrl).into(circleImageView);

        buttonLogout = findViewById(R.id.button_logout);

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean("userLogin", false);
                editor.apply();

                LoginManager.getInstance().logOut();
                Intent fblogin = new Intent(HomeScreenActivity.this, LoginActivity.class);
                startActivity(fblogin);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
