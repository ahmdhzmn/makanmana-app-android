package com.makanmanaapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EditProfileActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;

    String id, email, username, profilename, userType, userTypeDescription, userLocation;
    TextView txtUsername, txtProfilename, txtBigProfilename, txtUserLocation;

    Button saveButton, cancelButton;
    Toolbar toolbar;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference RootRef = FirebaseDatabase.getInstance().getReference();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);


        sharedpreferences = getSharedPreferences("userPref", Context.MODE_PRIVATE);

        id = sharedpreferences.getString("userId", "");
        email = sharedpreferences.getString("userEmail", "");
        username = sharedpreferences.getString("userName", "");
        profilename = sharedpreferences.getString("profileName", "");
        userType = sharedpreferences.getString("userType", "");
        userLocation = sharedpreferences.getString("userLocation", "");

        if (userType.equals("1")) {
            userTypeDescription = "Guest User";
        }
        else
            userTypeDescription = "Facebook User";

        txtUsername = findViewById(R.id.edit_username);
        txtProfilename = findViewById(R.id.edit_profile_name);
        txtBigProfilename = findViewById(R.id.profilescreen_profname);
        txtUserLocation = findViewById(R.id.profilescreen_location);
        saveButton = findViewById(R.id.edit_save);
        cancelButton = findViewById(R.id.edit_cancel);

        txtUsername.setHint(username);
        txtProfilename.setHint(profilename);
        txtBigProfilename.setText(profilename);
        txtUserLocation.setText(userLocation);

        toolbar = findViewById(R.id.toolbar_edit_profile);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Toast.makeText(EditProfileActivity.this,"Saving..", Toast.LENGTH_LONG).show();

                final String username = txtUsername.getText().toString().trim();
                final String profilename = txtProfilename.getText().toString().trim();;
                if (TextUtils.isEmpty(username) && TextUtils.isEmpty(profilename)) {
                    Toast.makeText(EditProfileActivity.this,"Please enter any fields to update profile", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(username) && !TextUtils.isEmpty(profilename)) {
                    RootRef.child("Users").child(mAuth.getCurrentUser().getUid()).child("profilename").setValue(profilename).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()) {

                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("profileName", profilename);
                                editor.apply();


                                Intent mainScreen = new Intent(EditProfileActivity.this, MainActivity.class);
                                mainScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                Toast.makeText(EditProfileActivity.this, "Profile Name Succesfully Saved", Toast.LENGTH_LONG).show();

                                startActivity(mainScreen);
                                //finish();
                            }
                            else {
                                Toast.makeText(EditProfileActivity.this, "Update Error! Please try again later.", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                } else if (!TextUtils.isEmpty(username) && TextUtils.isEmpty(profilename)) {
                    RootRef.child("Users").orderByChild("username").equalTo(username).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getChildrenCount() > 0) {
                                Toast.makeText(getApplicationContext(), "Username already exists. Please try other username.", Toast.LENGTH_LONG).show();
                            }
                            else {
                                RootRef.child("Users").child(mAuth.getCurrentUser().getUid()).child("username").setValue(username).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()) {

                                            SharedPreferences.Editor editor = sharedpreferences.edit();
                                            editor.putString("userName", username);
                                            editor.apply();

                                            Intent mainScreen = new Intent(EditProfileActivity.this, MainActivity.class);
                                            mainScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            Toast.makeText(EditProfileActivity.this, "Username Succesfully Saved", Toast.LENGTH_LONG).show();
                                            startActivity(mainScreen);
                                            //  finish();
                                        }
                                        else {
                                            Toast.makeText(EditProfileActivity.this, "Update Error! Please try again later.", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                } else if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(profilename)) {
                    RootRef.child("Users").orderByChild("username").equalTo(username).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getChildrenCount() > 0) {
                                Toast.makeText(getApplicationContext(), "Username already exists. Please try other username.", Toast.LENGTH_LONG).show();
                            }
                            else {
                                RootRef.child("Users").child(mAuth.getCurrentUser().getUid()).child("username").setValue(username);
                                RootRef.child("Users").child(mAuth.getCurrentUser().getUid()).child("profilename").setValue(profilename);

                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("profileName", profilename);
                                editor.putString("userName", username);
                                editor.apply();

                                Intent mainScreen = new Intent(EditProfileActivity.this, MainActivity.class);
                                mainScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                Toast.makeText(EditProfileActivity.this,"Username & Profile Name Succesfully Saved", Toast.LENGTH_LONG).show();
                                startActivity(mainScreen);
                                //finish();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            //  Toast.makeText(EditProfileActivity.this,"Back..", Toast.LENGTH_LONG).show();

            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
