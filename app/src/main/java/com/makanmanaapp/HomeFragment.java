package com.makanmanaapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeFragment extends Fragment {

    SharedPreferences sharedpreferences;

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    DatabaseReference RootRef = FirebaseDatabase.getInstance().getReference();

    String id, email, profilename, imageUrl, userLocation, userCoordinates;

    CircleImageView circleImageView;

    TextView welcomeText, txtUserId, txtProfileName, txtUserEmail, txtUserCoordinates, txtUserLocation;

    FloatingActionButton buttonLogout;

    View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        FacebookSdk.sdkInitialize(view.getContext());

//        if (currentUser != null) {
//
//            RootRef.child("Users").child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
//                @Override
//                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                    txtUserEmail.setText(dataSnapshot.child("email").getValue().toString());
//                    txtProfileName.setText(dataSnapshot.child("profilename").getValue().toString());
//
//                }
//
//                @Override
//                public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                }
//            });
//        }

        // circleImageView = view.findViewById(R.id.home_profile_image);
        txtUserEmail = view.findViewById(R.id.home_userEmail);
        txtProfileName = view.findViewById(R.id.home_profileName);
        txtUserCoordinates = view.findViewById(R.id.home_userCoordinates);
        txtUserLocation = view.findViewById(R.id.home_userLocation);

        sharedpreferences = getActivity().getSharedPreferences("userPref", Context.MODE_PRIVATE);




        Boolean isLogin = sharedpreferences.getBoolean("isLogin", false);
        id = sharedpreferences.getString("userId", "");
        email = sharedpreferences.getString("userEmail", "");
        profilename = sharedpreferences.getString("profileName", "");
        userLocation = sharedpreferences.getString("userLocation", "");
        userCoordinates = sharedpreferences.getString("userCoordinates", "");

        RoundCornerDrawable round = new RoundCornerDrawable(BitmapFactory.decodeResource(getResources(),R.drawable.background_card),
                getResources().getDimension(R.dimen.cardview_radius), 0);
        ImageView imageView = view.findViewById(R.id.backgroundimage);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            imageView.setBackground(round);
        else
            imageView.setBackgroundDrawable(round);

        txtUserEmail.setText(email);
        txtProfileName.setText(profilename);
        txtUserCoordinates.setText(userCoordinates);
        txtUserLocation.setText(userLocation);


//        if (isLogin == true) {
//            txtUserEmail.setText(email);
//            txtProfileName.setText(profilename);
//            txtUserCoordinates.setText(userCoordinates);
//            txtUserLocation.setText(userLocation);
//        } else {
//            txtUserEmail.setText("");
//            txtProfileName.setText("");
//            txtUserCoordinates.setText("");
//            txtUserLocation.setText("");
//        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.dontAnimate();

        //Glide.with(HomeFragment.this).load(imageUrl).into(circleImageView);

        FloatingActionButton buttonLogout = view.findViewById(R.id.button_logout);
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();

                sharedpreferences = getActivity().getSharedPreferences("userPref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("userId", "");
                editor.putString("userEmail", "");
                editor.putString("userName", "");
                editor.putString("profileName", "");
                editor.putString("userType", "");
                editor.putString("userLocation", "");
                editor.putString("userCoordinates", "");
                editor.putString("mainPreference", "");
                editor.putInt("mainPreferenceValuePosition", 0);
                editor.putString("secondPreference", "");
                editor.putInt("secondPreferenceValuePosition", 0);
                editor.putString("thirdPreference", "");
                editor.putInt("thirdPreferenceValuePosition", 0);
                editor.putString("fourthPreference", "");
                editor.putInt("fourthPreferenceValuePosition", 0);
                editor.putInt("countGenerateRecommendation", 0);
                editor.putString("userNumberOfRecommend", "");
                editor.putBoolean("isLogin", false);
                editor.apply();

                Intent loginScreen = new Intent(getContext(), LoginActivity.class);
                loginScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginScreen);
            }
        });

        return view;
    }

}
