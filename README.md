# MakanMana Android Mobile App

Restaurant Recommendation Mobile Application

**Features - Facebook Integration, Firebase Authentication, Google APIs (Places, Maps, Distance Matrix), Realtime Chat**


![User Interfaces](https://i.ibb.co/whwqMYG/interface-collage.jpg)


Using content-based recommender concept and group aggregation strategy, this app recommends top restaurants to a group of users based on their preferences according to their preferred location.

User can login through their facebook account or sign up as guest user. For facebook user, their likes data will be fetched using Facebook Graph API and the data will be analyzed through MakanMana's Laravel web server's (back end) filtering engine.

For guest users, they must set their preferences after their first login. Guest users also can change their preferences anytime they want.

By implementing researched algorithm, the filtering engine for this apllication will give group preference results. Google APIs are used to give restaurants data in specific places and the application will displayed recommended restaurants based on the retrieved data.